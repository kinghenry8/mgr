#!/bin/env python2.7
#--------------------------------
# vi: sw=4 ts=4 expandtab ai
#--------------------------------
# $Id: hoagies.py 70 2012-08-16 23:37:45Z pytools $
#--------------------------------

hoagiesMenu =  {#hoagies menu begin
    "7_hoagies" : {#section begin
        "meta" : {#meta begin
             "sTitle" : "Hoagies:Hoagies" ,            
             "blurb" : "All our hoagies and grinders are made to order. So have it your way<br> <b>GRINDERS:</b> Any of our delicious hoagies can be made into a grinder. Hoagies are toasted in the oven, steaming the vegetables, and melting the cheese on top - resulting in this uniquely tasting sandwich!"
        } ,#meta end

        "Cheese" : {#orderable item begin
            "iTitle" : "Cheese Hoagie",
           "m_tags"  : (  "vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.00", ) ,
            }#pricing end
        } ,#orderable item end

        "Italian" : {#orderable item begin
            "iTitle" : "Italian Hoagie",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
        "Ham and Cheese" : {#orderable item begin
            "iTitle" : "Ham and Cheese Hoagie",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
        "Turkey_and_Cheese" : {#orderable item begin
            "iTitle" : "Turkey and Cheese Hoagie",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
        "Veal_Parmesan" : {#orderable item begin
            "iTitle" : "Veal Parmesan Hoagie",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
        "Grilled_Chicken" : {#orderable item begin
            "iTitle" : "Grilled Chicken Hoagie",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.75", ) ,
            }#pricing end
        } ,#orderable item end,
        "Tuna_and_Cheese" : {#orderable item begin
            "iTitle" : "Tuna and Cheese Hoagie",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
        "Regular" : {#orderable item begin
            "iTitle" : "Regular Hoagie",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
        "Salami_and_Cheese" : {#orderable item begin
            "iTitle" : "Salami and Cheese Hoagie",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
       "Meatball" : {#orderable item begin
            "iTitle" : "Meatball Hoagie",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
       "Chicken_Parmesan" : {#orderable item begin
            "iTitle" : "Chicken Parmesan Hoagie",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
       "Fresh_Flounder" : {#orderable item begin
            "iTitle" : "Fresh Flounder Hoagie",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.75", ) ,
            }#pricing end
        } ,#orderable item end
    }#section end
}#hoagies menu end
