#!/bin/env python2.7
#--------------------------------
# vi: sw=4 ts=4 expandtab ai
#--------------------------------
#$Id: mgrOrdEntry2.py 64 2012-06-10 23:59:04Z pytools $
#--------------------------------
import os,sys,pprint
#pp=pprint.PrettyPrinter(indent=4)
import logging.handlers, logging.config
import time, inspect,re,pdb
import shelve
#-------- SDR imports ---------------
from pyCustom import PyCustom
myTopDir=PyCustom.addOurPaths()
from infra.pyLibs import genUtils  as gu

slideBank= {
    "group1" : """
        <img src="/DVR/apps/MGR2/pics/wrap_1.jpg" title="wrap-1" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/wrap_2.jpg" title="wrap-2" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/wrap_3.jpg" title="wrap-3" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/wrap_4.jpg" title="wrap-4" class='mgrImg'>
    """,

    "group2" : """
        <img src="/DVR/apps/MGR2/pics/wrap_5.jpg" title="wrap-5" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/wrap_6.jpg" title="wrap-6" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/hoagie_1.jpg" title="hoagie-1" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/hoagie_2.jpg" title="hoagie-2" class='mgrImg'>
    """,

    "group3" : """
        <img src="/DVR/apps/MGR2/pics/hoagie_3.jpg" title="hoagie-3" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/hoagie_4.jpg" title="hoagie-4" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/hamburger_1.jpg" title="hamburger-1" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/hamburger_2.jpg" title="hamburger-2" class='mgrImg'>
    """,

    "group4" : """
        <img src="/DVR/apps/MGR2/pics/tacos_1.jpg" title="tacos-1" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/tacos_2.jpg" title="tacos-2" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/tacos_3.jpg" title="tacos-3" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/tacos_4.jpg" title="tacos-4" class='mgrImg'>
    """,

    "group5" : """
        <img src="/DVR/apps/MGR2/pics/wings_1.jpg" title="wings-1" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/wings_2.jpg" title="wings-2" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/side_sampler_1.jpg" title="sideSampler-1" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/side_sampler_2.jpg" title="sideSampler-2" class='mgrImg'>
    """,

    "group6" : """
        <img src="/DVR/apps/MGR2/pics/side_sampler_3.jpg" title="sideSampler-3" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/side_sampler_4.jpg" title="sideSampler-4" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/side_sampler_5.jpg" title="sideSampler-5" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/chick_cheesesteak_1.jpg" title="chicken_cheeseSteak_1" class='mgrImg'>
    """,

    "group7" : """
        <img src="/DVR/apps/MGR2/pics/sausagepepperoni_1.jpg" title="sausagepepperoni-1" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/sausagepepperoni_2.jpg" title="sausagepepperoni-2" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/sausage_1.jpg" title="sausage-1" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/pepperoni_1.jpg" title="pepperoni-1" class='mgrImg'>
    """,

    "group8" : """
        <img src="/DVR/apps/MGR2/pics/club_sand_1.jpg" title="clubSandwich-1" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/club_sand_2.jpg" title="clubSandwich-2" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/club_sand_3.jpg" title="clubSandwich-3" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/club_sand_4.jpg" title="clubSandwich-4" class='mgrImg'>
    """,

    "group9" : """
        <img src="/DVR/apps/MGR2/pics/cheesepizza_1.jpg" title="cheesepizza-1" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/cheesepizza_2.jpg" title="cheesepizza-2" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/cheesesteak_1.jpg" title="cheesesteak-1" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/cheesesteak_2.jpg" title="cheesesteak-2" class='mgrImg'>
    """,

    "group10" : """
        <img src="/DVR/apps/MGR2/pics/angelhair_1.jpg" title="angelhair-1" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/angelhair_2.jpg" title="angelhair-2" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/perogies_1.jpg" title="perogies-1" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/perogies_2.jpg" title="perogies-2" class='mgrImg'>
    """,
    "group11" : """
        <img src="/DVR/apps/MGR2/pics/meatballsub_1.jpg" title="meatballsub-1" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/meatballsub_2.jpg" title="meatballsub-2" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/hamburger_1.jpg" title="hamburger-1" class='mgrImg'>
        <img src="/DVR/apps/MGR2/pics/hamburger_2.jpg" title="hamburger-2" class='mgrImg'>
    """
}
 
 
class  SlideManager(object) :
    def __init__(self, numImgs=4, imgWidth=90, imgHt=80 ) :
        self.numImgs= numImgs
        self.imgWidth= imgWidth
        self.imgHt = imgHt
        self.slideWidth=numImgs*(imgWidth+12*2)
        self.slideHt=imgHt+2*9
        self.slideMarginLeft=50 
        self.slideMarginTop=8 

        self.sldShowWidth=self.slideWidth+ 20*2
        self.sldShowHt=self.slideHt

    #--------------------------------------------
    def initSlideManager(self) :
        print """
      <!DOCTYPE html>
      <style type="text/css">
          .slide { margin-left: %spx; margin-top: %spx;   border: solid 0px orange; width: %spx; height: %spx; overflow: auto  }
          .slideshow { width: %spx ;  height: %spx;  margin: auto; padding: 5px; border: 1px solid #ccc; background-color: #eee; }
          .mgrImg  { width: %spx;  height: %spx; padding: 5px ; border : 1px solid #ccc;  background-color: #eee }
      </style>
"""  %  (self.slideMarginLeft, self.slideMarginTop, self.slideWidth, self.slideHt,
         self.sldShowWidth, self.sldShowHt,
         self.imgWidth,  self.imgHt
        )

        print """
      <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
      <script type="text/javascript" src="http://cloud.github.com/downloads/malsup/cycle/jquery.cycle.all.latest.js"></script>
"""
 
    #--------------------------------------------
    def  initSlideFlipper(self) :
        return """
          <script>
          $(function() {
              $('.slideshow').cycle( {
                  timeout: 0 ,
                  speed : 200 ,
                  fx: 'fadeZoom'
                  }
              )

              $('.pixie').click (function() {
                  grpNum=parseInt($(this).attr('grp') )
                  console.log(grpNum)
                  $('.slideshow').cycle(grpNum)
                  return false
              })

          })
          </script>
          """
    #--------------------------------------------
    def initSlideFlipperMenu(self) :
        print """
          <div style="position:absolute; text-align:center; left: 0px; top: 250px; width: 100;">
          <span class='pixie' grp='0'>Wraps </span> <br>
          <span class='pixie' grp='1'>Hoagies <span><br>
          <span class='pixie' grp='2'>Tacos2<span><br>
          <span class='pixie' grp='3'>Tacos3<span><br>
          <span class='pixie' grp='4'>Tacos4<span><br>
          <span class='pixie' grp='5'>Tacos5<span><br>
          <span class='pixie' grp='6'>Tacos6<span><br>
          <span class='pixie' grp='7'>Tacos7<span><br>
          <span class='pixie' grp='8'>Tacos8<span><br>
          <span class='pixie' grp='9'>Tacos9 <span><br>
          <span class='pixie' grp='10'>Tacos10 <span><br>
          </div>
"""
      #--------------------------------------------

    def  initSlides(self) :
        print """
<div  class='slideshow'>
    <div class='slide'> %s </div>
    <div class='slide'> %s </div>
    <div class='slide'> %s </div>
    <div class='slide'> %s </div>
    <div class='slide'> %s </div>
    <div class='slide'> %s </div>
    <div class='slide'> %s </div>
    <div class='slide'> %s </div>
    <div class='slide'> %s </div>
    <div class='slide'> %s </div>
    <div class='slide'> %s </div>
</div>
"""  % ( 
    slideBank['group1'], slideBank['group2'], slideBank['group3'], 
    slideBank['group4'], slideBank['group5'], slideBank['group6'], 
    slideBank['group7'], slideBank['group8'], slideBank['group9'], 
    slideBank['group10'], slideBank['group11'],
    )


#--------------------------------------------------
if __name__ == "__main__" :
    smgr=SlideManager()         
    smgr.initSlideManager()          # styles / js / div                      
    print smgr.initSlideFlipper()    # interaction  logic
    smgr.initSlideFlipperMenu()      # clickable links
    smgr.initSlides()                # load slide images

