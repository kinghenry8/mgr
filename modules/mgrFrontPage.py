#!/bin/env python2.7
#--------------------------------
# vi: sw=4 ts=4 expandtab ai
#  $Id: mgrFrontPage.py 65 2012-07-01 02:05:28Z pytools $
#--------------------------------
import os,sys,pprint
import logging.handlers, logging.config
import time, inspect,re,pdb
from pyCustom import PyCustom
myTopDir=PyCustom.addOurPaths()
from infra.pyLibs import genUtils  as gu
from infra.pyLibs.cgiApp import CgiApp
from infra.pyLibs.clientUtils import ClientUtils
from mgrLayouts import MgrLayouts
from mgrOrdEntry2 import MgrOrderEntry 

myTopDir=PyCustom.addOurPaths()
pp=pprint.PrettyPrinter(indent=4)
# from apps.MGR.modules.mgrOrdEntry  import MgrOrderEntry as MgrOE
#------------------------------------------------------
validArgs= {
    "cgiAction" : {
        "cliKey" : "gAction",
        "help"   : "Allowed Actions",
        "type"   : "oneof|DO_TAB_LAYOUT|DO_OELAYOUT|FRONT_PAGE|LEFT_MENU|SEC_MENU|BANNER|LEAF_MENU",
        "sample" : "cgiAction=TEST_DATES",
        "default" : "DO_TAB_LAYOUT"
    } , 
    "cgiTrdDate" : {
        "cliKey" : "gTrdDate",
        "help"   : "Trade (business) date",
        "type"   : "yyyymmdd",
        "sample" : "20110303",
        "default" : "20110305"
    } ,
    "cgiSecId" : {
        "cliKey" : "gSecId",
        "help"   : "menu section name",
        "type"   : "string" ,
        "sample" : "stromboli",
        "default" : "1_classic"
    } ,
    "cgiOrderItem" : {
        "cliKey" : "gOrderItem",
        "help"   : "leaf menu name",
        "type"   : "string" ,
        "sample" : "Vegetarian",
        "default" : "pizza",
    } ,
    "cgiQuantity" : {
        "cliKey" : "gQuantity",
        "help"   : "order quantity",
        "type"   : "int" ,
        "sample" : "2",
        "default" : "1"
    } ,
    "cgiUserId" : {
        "cliKey" : "gUserId",
        "help"   : "user id for cart",
        "type"   : "string" ,
        "sample" : "guest",
        "default" : "guest"
    } ,
    "cgiItemId" : {
        "cliKey" : "gItemId",
        "help"   : "one item from the cart",
        "type"   : "string" ,
        "sample" : "1",
        "default" : "0"
    } ,
    "cgiSizeType" : {
        "cliKey" : "gSizeType",
        "help"   : "is it small/large/oneSize", 
        "type"   : "string" ,
        "sample" : "onesize",
        "default" : "small"
    } ,
    "cgiItemsPrice" : {
        "cliKey" : "gItemsPrice",
        "help"   : "price of one cart entry",
        "type"   : "float" ,
        "sample" : "5.0",
        "default" : "46.5"
    } ,

    "cgiItemsDetail" : {
        "cliKey" : "gItemsDetail",
        "help"   : "details of one cart entry",
        "type"   : "string" ,
        "sample" : "pizza thincrust toppings: mushroom, onion",
        "default" : "Section: 1_classic|Type: thin%20crust|Toppings: peppers,spinach"
    },

    "cgiIsIE" : {
        "cliKey" : "gIsIE",
        "help"   : "is this an IE ?",
        "type"   : "string (y/n)" ,
        "sample" : "y",
        "default" : "n"
    },

    "cgiOrderInfo" : {
        "cliKey" : "gOrderInfo",
        "help"   : "order contact info and other details (at commit)",
        "type"   : "string" ,
        "sample" : "me@mail.com|908-555-1212|deltype|paytype",
        "default" : "dr@gmail.com|215-333-4444|delivery|cash",
    } ,

}
#-----------------------------------
def processAppActions() :
    mgroe2=MgrOrderEntry()
    if  cliValues['gAction'] == 'DO_TAB_LAYOUT' :
        tabsHtm=myTopDir + "/apps/MGR2/modules/mainTabs.htm"
        MgrLayouts.doTabLayout(tabsHtm)
    elif  cliValues['gAction'] == 'DO_OE_LAYOUT' :
        oeLayHtm=myTopDir + "/apps/MGR2/modules/mgrOElo.html"
        MgrLayouts.doOrderEntryLayout(oeLayHtm)
    elif  cliValues['gAction'] == 'FRONT_PAGE' :
        fpageHtm=myTopDir + "/apps/MGR2/modules/mgrFP.htm"
        MgrLayouts.doFrontPage(fpageHtm)
    elif  cliValues['gAction'] == 'DO_CAROUSEL' :
        mgroe2.initCarousel()

    elif cliValues['gAction'] == 'MAIN_MENU' :
        mgroe2.refreshTopMenu( userId=cliValues['gUserId'], script=cliValues['gScriptUrl'])

    elif cliValues['gAction'] == 'SEC_MENU' :
        mgroe2.renderSectionMenu( section=cliValues['gSecId'], userId=cliValues['gUserId'])
    elif cliValues['gAction'] == 'LEAF_MENU' :
        mgroe2.renderLeafMenu( sizeType=cliValues['gSizeType'], orderQty=cliValues['gQuantity'],
                section=cliValues['gSecId'], orderItem=cliValues['gOrderItem'], 
                userId=cliValues['gUserId'] )
    elif cliValues['gAction'] == 'ADD2_CART' :
        #&cgiQuantity=6&cgiItemsDetail=Type@thin%20crust|Toppings@peppers:spinach:&cgiItemsPrice=46.5 
        mgroe2.addOneItemToCart(
            quantity=cliValues['gQuantity'], itemsDetail=cliValues['gItemsDetail'], 
            itemsPrice=cliValues['gItemsPrice'], userId=cliValues['gUserId']
            )
    elif cliValues['gAction'] == 'SHOW_CART' :
        print ClientUtils.emitSimpleHtml()
        print ClientUtils.emitJqHeaders()
        mgroe2.renderUserCart(userId=cliValues['gUserId'] )
    elif cliValues['gAction'] == 'CLEAR_CART' :
        #&cgiQuantity=6&cgiItemsDetail=Type@thin%20crust|Toppings@peppers:spinach:&cgiItemsPrice=46.5 
        mgroe2.clearUserCart( userId=cliValues['gUserId'])
    elif cliValues['gAction'] == 'GEN_UNIQID' :
        mgroe2.generateUniqId(cliValues)
    elif cliValues['gAction'] == 'DEL_CART_ITEM' :
        mgroe2.delCartItem( userId=cliValues['gUserId'], itemId=cliValues['gItemId'] )

    elif cliValues['gAction'] == 'CHECKOUT_CART' :
        mgroe2.checkoutCart( userId=cliValues['gUserId'] )
    elif cliValues['gAction'] == 'ORDER_COMMIT' :
        mgroe2.orderCommit( userId=cliValues['gUserId'], orderInfo=cliValues['gOrderInfo'] )
    elif cliValues['gAction'] == 'SHOW_ORDERS' :
        mgroe2.renderOrdersForUserId( userId=cliValues['gUserId'] )
        
#-----------------------------------
def processMain() :
    global cliValues
    if gu.showArgsHelp(validArgs)  :
        sys.exit(0)
    cliValues=CgiApp.processWebArgs(validArgs)
    if cliValues['gAction'] == 'help' :
        print ClientUtils.emitSimpleHtml()
        CgiApp.showHelp(cliValues['gScriptUrl'], validArgs)
        sys.exit(0)
    processAppActions()

#-----------------------------
if __name__ == "__main__" :
    logging.config.fileConfig(os.path.expanduser("~") + "/appConf/log4py.conf")
    processMain()
