#!/bin/env python2.7
#--------------------------------
# vi: sw=4 ts=4 expandtab ai
#--------------------------------
# $Id: stromboliMenus.py 70 2012-08-16 23:37:45Z pytools $
#--------------------------------

stromboliMenu =  {
    "3_strombolis" : {
        "meta" : {
             "sTitle" : "Strombolis: Try our Stromboli" ,
             "blurb" : "All made with original sauce with herbs and spices & cheese",
             "sizes" : "<b>small</b> serves 1-2 <b>large</b> serves 2-4",
        } ,

        "Regular" : {
            "iTitle" : "Regular",
           "m_tags"  : ( "vegetarian" ),
             "blurb" :  "sauce and cheese",
           "pricing" :  {
              "0_sizes" : ("small:7.0", "large:10.0" ) ,
            }
        } ,

        "Ham" : {
             "iTitle" : "Ham",
             "blurb" :  "",
           "pricing" :  {
              "0_sizes" : ("small:7.5", "large:11.5"),
            }
        } ,
        "Steak" : {
             "iTitle" : "Steak",
             "blurb" :  "",
           "pricing" :  {
              "0_sizes" : ("small:8.50", "large:14.50")
            }
        } ,
        "Pepperoni" : {
             "iTitle" : "Pepperoni",
             "blurb" :  "",
           "pricing" :  {
              "0_sizes" : ("small:7.5", "large:11.5"),
            }
        } ,
        "Meatball" : {
             "iTitle" : "Meatball",
             "blurb" :  "",
           "pricing" :  {
              "0_sizes" : ("small:7.5", "large:11.5"),
            }
        } ,
        "Chicken" : {
             "iTitle" : "Chicken",
             "blurb" :  "",
           "pricing" :  {
              "0_sizes" : ("small:8.50", "large:13.0"),
            }
        } ,
        "Sausage" : {
             "iTitle" : "Sausage",
             "blurb" :  "",
           "pricing" :  {
              "0_sizes" : ("small:7.5", "large:11.5")
            }
        } ,
        "Mushroom" : {
             "iTitle" : "Mushroom",
             "blurb" :  "",
           "pricing" :  {
              "0_sizes" : ("small:7.5", "large:11.5"),
            }
        } ,
        "Broccoli" : {
             "iTitle" : "Broccoli",
             "blurb" :  "",
           "pricing" :  {
              "0_sizes" : ("small:7.5", "large:11.5")
            }
        } ,
    }
}
#================================================
gourmetStromboliMenu = {
    "4_gourmetStrombolis" : {
        "meta" : {
             "sTitle" : " Gourmet Stromboli : Try a Mad Greek Gourmet Stromboli or Calzone" ,
             "blurb" : "Calzones are similar to Strombolis, however made with Ricotta cheese (instead of sauce, but served on the side)",
        },

        "Vegetarian" : {
            "iTitle" : "Vegetarian",
            "m_tags" : ("vegetarian",),
            "blurb" : "Spinach,tomato,mushroom,broccoli,onion & green peppers",
            "pricing" : {
                 "0_sizes" : ("small:9.00", "large:14.0"),
                 "1_calzone" : {
                     "small" :  ("yes:1.0", "no:0") ,
                     "large" :  ("yes:2.0", "no:0")
                 }
            }
        } ,

        "Greek" : {
            "iTitle" : "Greek",
            "m_tags" : ("vegetarian",),
            "blurb" : "Feta cheese, spinach, tomato, olives",
            "pricing" : {
                 "0_sizes" : ("small:8.50", "large:13.50"),
                 "1_calzone" : {
                     "small" :  ("yes:1.0", "no:0") ,
                     "large" :  ("yes:2.0", "no:0")
                 }
            }
           } ,        
         "Spinach Pie" : {
            "iTitle" : "Spinach Pie",
            "m_tags" : ("vegetarian",),
            "blurb" : "Garden style spinach, onion, & feta cheese",
            "pricing" : {
                 "0_sizes" : ("small:7.75", "large:13.0"),
                 "1_calzone" : {
                     "small" :  ("yes:1.0", "no:0") ,
                     "large" :  ("yes:2.0", "no:0")
                 }
            }
        } ,
        "Special" : {
            "iTitle" : "Special",
            "m_tags" : ("non-vegetarian",),
            "blurb" : "Steak, pepperoni, mushroom, onion, & sweet peppers",
            "pricing" : {
                 "0_sizes" : ("small:9.00", "large:14.0"),
                 "1_calzone" : {
                     "small" :  ("yes:1.0", "no:0") ,
                     "large" :  ("yes:2.0", "no:0")
                 }
            }
        } ,
         "Italian" : {
            "iTitle" : "Italian",
            "m_tags" : ("non-vegetarian",),
            "blurb" : "Ham, Genoa salami, capicola, & pepperoni",
            "pricing" : {
                 "0_sizes" : ("small:8.50", "large:13.0"),
                 "1_calzone" : {
                     "small" :  ("yes:1.0", "no:0") ,
                     "large" :  ("yes:2.0", "no:0")
                 }
            }
        } ,
         "Western" : {
            "iTitle" : "Western",
            "m_tags" : ("non-vegetarian",),
            "blurb" : "Ham, feta cheese, & green pepper",
            "pricing" : {
                 "0_sizes" : ("small:7.75", "large:13.0"),
                 "1_calzone" : {
                     "small" :  ("yes:1.0", "no:0") ,
                     "large" :  ("yes:2.0", "no:0")
                 }
            }
        } ,
       "Buffalo Chicken Stromboli" : {
            "iTitle" : "Buffalo Chicken",
            "m_tags" : ("non-vegetarian",),
            "blurb" : "Chicken, mozzarella, blue cheese, & pizza sauce",
            "pricing" : {
                 "0_sizes" : ("small:8.50", "large:13.50"),
                 "1_calzone" : {
                     "small" :  ("yes:1.0", "no:0") ,
                     "large" :  ("yes:2.0", "no:0")
                 }
            }
        } ,      
         "Special Chicken" : {
            "iTitle" : "Special Chicken",
            "m_tags" : ("non-vegetarian",),
            "blurb" : "Mushroom, onion, anchovies & sweet peppers",
            "pricing" : {
                 "0_sizes" : ("small:9.00", "large:14.00"),
                 "1_calzone" : {
                     "small" :  ("yes:1.0", "no:0") ,
                     "large" :  ("yes:2.0", "no:0")
                 }
            }
        } ,
                             
    }
}
#---------------------------------------------
if  __name__ == '__main__' :
    import os,sys, pprint
    pp=pprint.PrettyPrinter(indent=4)
    pp.pprint (stromboliMenu)
    pp.pprint (gourmetStromboliMenu)
