#!/bin/env python2.7
#--------------------------------
# vi: sw=4 ts=4 expandtab ai
#--------------------------------
# $Id: hamburgers.py 70 2012-08-16 23:37:45Z pytools $
#--------------------------------

hamburgerMenu =  {#hamburger menu begin
    "8_hamburgers" : {#section begin
        "meta" : {#meta begin
             "sTitle" : "Hamburgers:Hamburgers" ,            
             "blurb" : "All our hamburgers are made from fresh ground beef never frozen patties"
        } ,#meta end

        "Hamburger" : {#orderable item begin
            "iTitle" : "Hamburger",
           "m_tags"  : (  "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:3.99", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Lettuce and Tomato:0.30"
                          ),
            }#pricing end
        } ,#orderable item end

        "Bacon_Burger" : {#orderable item begin
            "iTitle" : "Bacon Burger",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:4.50", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Lettuce and Tomato:0.30"
                          ),
            }#pricing end
        } ,#orderable item end
        "Pizza_Burger" : {#orderable item begin
            "iTitle" : "Pizza Burger",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:4.50", ) ,
            }#pricing end
        } ,#orderable item end
        "Double_Cheeseburger" : {#orderable item begin
            "iTitle" : "Double Cheeseburger",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Lettuce and Tomato:0.30"
                          ),
            }#pricing end
        } ,#orderable item end
        "Cheeseburger" : {#orderable item begin
            "iTitle" : "Cheeseburger",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:4.25", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Lettuce and Tomato:0.30"
                          ),
            }#pricing end
        } ,#orderable item end
        "Bacon_Cheeseburger" : {#orderable item begin
            "iTitle" : "Bacon Cheeseburger",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:4.75", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Lettuce and Tomato:0.30"
                          ),
            }#pricing end
        } ,#orderable item end,
        "Double_Hamburger" : {#orderable item begin
            "iTitle" : "Double Hamburger",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.00", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Lettuce and Tomato:0.30"
                          ),
            }#pricing end
        } ,#orderable item end
        "Cheese_Burger_Platter" : {#orderable item begin
            "iTitle" : "Cheese Burger Platter",
           "m_tags"  : ( "non-vegetarian", "platter" ),
             "blurb" :  "Cheeseburger with fries",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.25", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Lettuce and Tomato:0.30"
                          ),
            }#pricing end
        } ,#orderable item end
      "Cheeseburger Hoagie" : {#orderable item begin
            "iTitle" : "Cheeseburger Hoagie",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Lettuce and Tomato:0.30"
                          ),
            }#pricing end
        } ,#orderable item end
      "Sliders with French Fries" : {#orderable item begin
            "iTitle" : "3 pieces Burger Sliders with French Fries",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Lettuce and Tomato:0.30"
                          ),
            }#pricing end
        } ,#orderable item end
    }#section end
}#burger menu end
