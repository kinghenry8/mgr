#------------------------------
#$Id: pasta.py 71 2012-08-19 19:32:25Z pytools $
#------------------------------
# vi: sw=4 ts=4 expandtab ai
#------------------------------
pastaMenu =  {#pasta menu begin
    "95_pasta" : {#section begin
        "meta" : {#meta begin
             "sTitle" : "Platters:Platters" ,            
             "blurb" : ""
        } ,#meta end

        "Spaghetti_homemade_sauce" : {#orderable item begin
            "iTitle" : "Spaghetti with homemade sauce",
           "m_tags"  : ( "vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:5.75", ) ,
            }#pricing end
        } ,#orderable item end

        "Spaghetti_meatballs" : {#orderable item begin
            "iTitle" : "Spaghetti with meatballs",
            "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.75", ) ,
            }#pricing end
        } ,#orderable item end
        "Veal_parmigiana_spaghetti" : {#orderable item begin
            "iTitle" : "Veal Parmigiana with Spaghetti",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:7.25", ) ,
            }#pricing end
        } ,#orderable item end
        "Chicken_Parmigiana_spaghetti" : {#orderable item begin
            "iTitle" : "Chicken parmigiana with Spaghetti",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:7.25", ) ,
            }#pricing end
        } ,#orderable item end
        "Cheese_Ravioli_sauce" : {#orderable item begin
            "iTitle" : "Cheese ravioli with sauce",
           "m_tags"  : ( "vegetarian" , ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:5.75", ) ,
            }#pricing end
        } ,#orderable item end
        "Cheese_ravioli_meatballs" : {#orderable item begin
            "iTitle" : "Cheese ravioli with meatballs",
           "m_tags"  : ( "non-vegetarian" , ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.75", ) ,
            }#pricing end
        } ,#orderable item end
        "Fresh Flounder Platter" : {#orderable item begin
            "iTitle" : "Fresh Flounder Platter",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:8.25", ) ,
            }#pricing end
        } ,#orderable item end
           "Fried Chicken Platter" : {#orderable item begin
            "iTitle" : "Fried Chicken Platter",
           "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "1/2 chicken with fries, cole slaw, lettuce and tomato",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:8.50", ) ,
            }#pricing end
        } ,#orderable item end
    }#section end
}#steak menu end
#---------------------------------------------
if  __name__ == '__main__' :
    import os,sys, pprint
    pp=pprint.PrettyPrinter(indent=4)
    pp.pprint (pastaMenu)
