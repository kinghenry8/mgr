#!/bin/env python2.7
#--------------------------------
# vi: sw=4 ts=4 expandtab ai
#--------------------------------
# $Id: steakSand.py 70 2012-08-16 23:37:45Z pytools $
#--------------------------------

steakSandMenu =  {#steak menu begin
    "5_steakSands" : {#section begin
        "meta" : {#meta begin
             "sTitle" : "Steak Sandwiches:Steak Sandwiches" ,            
             "blurb" : "CLassic Philly Cheese Steaks!"
        } ,#meta end

        "Plain_Steak" : {#orderable item begin
            "iTitle" : "Plain Steak",
           "m_tags"  : (  "cheesesteak", "non-vegetarian", "cheese steak" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.00", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Extra meat:2.50",
                          "Mushrooms:0.50", "Pepperoni:0.75"
                          ),
            }#pricing end
        } ,#orderable item end

        "Steak_Hoagie" : {#orderable item begin
            "iTitle" : "Steak Hoagie",
           "m_tags"  : ( "cheesesteak", "non-vegetarian", "cheese steak" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.00", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Extra meat:2.50",
                          "Mushrooms:0.50", "Pepperoni:0.75"
                          ),
            }#pricing end
        } ,#orderable item end
        "Mushroom_Steak" : {#orderable item begin
            "iTitle" : "Mushroom Steak",
           "m_tags"  : ( "cheesesteak", "non-vegetarian", "cheese steak" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.25", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Extra meat:2.50",
                          "Mushrooms:0.50", "Pepperoni:0.75"
                          ),
            }#pricing end
        } ,#orderable item end
        "Pepper_Steak" : {#orderable item begin
            "iTitle" : "Pepper Steak",
           "m_tags"  : ( "cheesesteak", "non-vegetarian", "cheese steak" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.25", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Extra meat:2.50",
                          "Mushrooms:0.50", "Pepperoni:0.75"
                          ),
            }#pricing end
        } ,#orderable item end
        "Pizza_Steak" : {#orderable item begin
            "iTitle" : "Pizza Steak",
           "m_tags"  : ( "cheesesteak", "non-vegetarian", "cheese steak" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Extra meat:2.50",
                          "Mushrooms:0.50", "Pepperoni:0.75"
                          ),
            }#pricing end
        } ,#orderable item end
        "CheeseSteak_Platter" : {#orderable item begin
            "iTitle" : "CheeseSteak Platter",
           "m_tags"  : ( "cheesesteak", "non-vegetarian", "cheese steak", "platter" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:8.50", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Extra meat:2.50",
                          "Mushrooms:0.50", "Pepperoni:0.75"
                          ),
            }#pricing end
        } ,#orderable item end,
        "CheeseSteak" : {#orderable item begin
            "iTitle" : "CheeseSteak",
           "m_tags"  : ( "cheesesteak", "non-vegetarian", "cheese steak" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.25", ) ,
            }#pricing end
        } ,#orderable item end
        "CheeseSteak_Hoagie" : {#orderable item begin
            "iTitle" : "CheeseSteak Hoagie",
           "m_tags"  : ( "cheesesteak", "non-vegetarian", "cheese steak" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Extra meat:2.50",
                          "Mushrooms:0.50", "Pepperoni:0.75"
                          ),
            }#pricing end
        } ,#orderable item end
        "Mushroom_CheeseSteak" : {#orderable item begin
            "iTitle" : "Mushroom CheeseSteak",
           "m_tags"  : ( "cheesesteak", "non-vegetarian", "cheese steak" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Extra meat:2.50",
                          "Mushrooms:0.50", "Pepperoni:0.75"
                          ),
            }#pricing end
        } ,#orderable item end
        "Pepper_CheeseSteak" : {#orderable item begin
            "iTitle" : "Pepper CheeseSteak",
           "m_tags"  : ( "cheesesteak", "non-vegetarian", "cheese steak" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
          "2m_options" : (
                          "Extra cheese:0.50", "Extra meat:2.50",
                          "Mushrooms:0.50", "Pepperoni:0.75"
                          ),
            }#pricing end
        } ,#orderable item end
       
    }#section end
}#steak menu end
#================================================
whtSteakSandMenu = {#White Steak Sandwiches begin
    "6_WhtStkSnd" : {#section begin
        "meta" : {#meta begin
             "sTitle" : "Chicken Steak Sandwiches: Chicken Steak Sandwiches" ,
             "blurb" : "",
        },#meta end

        "Chicken_Steak" : {#orderable item begin
            "iTitle" : "Chicken Steak",
           "m_tags"  : ( "chickensteak", "non-vegetarian", "chicken steak" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.00", ) ,
            }#pricing end
        } ,#orderable item end

        "Chicken_CheeseSteak" : {#orderable item begin
            "iTitle" : "Chicken CheeseSteak",
           "m_tags"  : ( "cheesesteak", "non-vegetarian", "chickensteak", "chicken steak" "cheese steak" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.25", ) ,
            }#pricing end
        } ,#orderable item end
        "Pizza_Chicken_Steak" : {#orderable item begin
            "iTitle" : "Pizza Chicken Steak",
           "m_tags"  : ( "cheesesteak", "non-vegetarian", "cheese steak", "chickensteak", "chicken steak" ),
             "blurb" :  "pizza cheese, & original sauce",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
        "Buffalo_Chicken_Steak" : {#orderable item begin
            "iTitle" : "Buffalo Chicken Steak",
           "m_tags"  : ( "cheesesteak", "non-vegetarian", "cheese steak", "chickensteak", "chicken steak" ),
             "blurb" :  "blue cheese & wing sauce",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
        "House_Special_Chicken_CheeseSteak" : {#orderable item begin
            "iTitle" : "House Special Chicken CheeseSteak",
           "m_tags"  : ( "cheesesteak", "non-vegetarian", "cheese steak", "chickensteak", "chicken steak" ),
             "blurb" :  "onion, mushroom, & green pepper",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
        "Chicken_CheeseSteak_Platter" : {#orderable item begin
            "iTitle" : "Chicken CheeseSteak Platter",
           "m_tags"  : ( "cheesesteak", "non-vegetarian", "cheese steak", "chickensteak", "chicken steak", "platter" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:8.50", ) ,
            }#pricing end
        } ,#orderable item end
    }#section end
}#gourmet menu end
#---------------------------------------------
if  __name__ == '__main__' :
    import os,sys, pprint
    pp=pprint.PrettyPrinter(indent=4)
    pp.pprint (steakSandMenu)
    pp.pprint (whtSteakSandMenu)
