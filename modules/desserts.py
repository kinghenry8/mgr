dessertsMenu =  {#sides menu begin
    "98_desserts" : {#section begin
        "meta" : {#meta begin
             "sTitle" : "Desserts:Desserts" ,            
             "blurb" : ""
        } ,#meta end

        "Carrot Cake, Chocolate Mousse or Cheese Cake" : {#orderable item begin
            "iTitle" : "Carrot Cake, Chocolate Mousse or Cheese Cake",
           "m_tags"  : ( "vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:2.25",) ,
            }#pricing end
        } ,#orderable item end

        "Fruit Smoothies (16 0z)" : {#orderable item begin
            "iTitle" : "16 0z. Fruit Smoothies",
            "m_tags"  : ( "vegetarian", ),
             "blurb" :  "(mango, banana, strawberry, pineapple) CHoose any flavor or combine any flavor",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:3.99",) ,
            }#pricing end
        } ,#orderable item end
    }#section end
}#sides menu end
#---------------------------------------------
if  __name__ == '__main__' :
    import os,sys, pprint
    pp=pprint.PrettyPrinter(indent=4)
    pp.pprint (dessertsMenu)
