#------------------------------
#$Id: wraps.py 71 2012-08-19 19:32:25Z pytools $
#------------------------------
# vi: sw=4 ts=4 expandtab ai
#------------------------------
wrapsMenu =  {#pita menu begin
    "92_wraps" : {#section begin
        "meta" : {#meta begin
             "sTitle" : "Wraps:Wraps" ,            
             "blurb" : ""
        } ,#meta end

        "Caesar_Wrap" : {#orderable item begin
            "iTitle" : "Caesar Wrap",
           "m_tags"  : (  "chicken", "non-vegetarian" ),
             "blurb" :  "Grilled Chicken, lettuce, tomato, Am. cheese, & caesar dressing",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end

        "Buffalo_Chicken_Wrap" : {#orderable item begin
            "iTitle" : "Buffalo Chicken Wrap",
            "m_tags"  : ( "chicken", "non-vegetarian", ),
             "blurb" :  "grilled chicken, blue cheese, & wing sauce",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
        "Cheeseburger_Wrap" : {#orderable item begin
            "iTitle" : "Cheeseburger Wrap",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "lettuce, tomato, & American cheese",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
        "Cheesesteak_Wrap" : {#orderable item begin
            "iTitle" : "Cheesesteak Wrap",
           "m_tags"  : ( "non-vegetarian" , "cheese" ),
             "blurb" :  "lettuce, tomato, fried onion, & American cheese",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
         "Chicken_Fajita_Wrap" : {#orderable item begin
            "iTitle" : "Chicken Fajita Wrap",
           "m_tags"  : ( "non-vegetarian" , "chicken" ),
             "blurb" :  "grilled chicken, fried onion, green pepper, cheese, & salsa",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
          "Flounder_Wrap" : {#orderable item begin
            "iTitle" : "Flounder Wrap",
           "m_tags"  : ( "non-vegetarian" , "flounder" ),
             "blurb" :  "lettuce, tomatio, onion, & tartar sauce",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
          "Veggie_Wrap" : {#orderable item begin
            "iTitle" : "Veggie Wrap",
           "m_tags"  : ( "vegetarian" ),
             "blurb" :  "lettuce, tomato, onion, mushroom, green pepper, spinach, broccoli, & cheese",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
          "3_Soft_Tacos" : {#orderable item begin
            "iTitle" : "3 Soft Tacos",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "lettuce, tomato, cheese & salsa on the side. Also served with nachos",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.75", ) ,
            }#pricing end
        } ,#orderable item end
          "Chicken_Finger_Wrap" : {#orderable item begin
            "iTitle" : "Chicken Finger Wrap",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "breaded chicken, lettuce, tomato, honey mustard",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
          "Good_Morning_Wrap" : {#orderable item begin
            "iTitle" : "Good Morning Wrap",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "toasted wrap stuffed w/ 3 eggs, your choice of sausage, bacon or ham, onion, gr. pepper and american cheese wrapped around fr. fries. Choose steak for 2.00 extra",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
    }#section end
}#steak menu end
#---------------------------------------------
if  __name__ == '__main__' :
    import os,sys, pprint
    pp=pprint.PrettyPrinter(indent=4)
    pp.pprint (wrapsMenu)
