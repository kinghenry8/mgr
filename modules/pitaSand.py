#------------------------------
#$Id: pitaSand.py 71 2012-08-19 19:32:25Z pytools $
#------------------------------
# vi: sw=4 ts=4 expandtab ai
#------------------------------
pitaSandMenu =  {#steak menu begin
    "91_pitaSands" : {#section begin
        "meta" : {#meta begin
             "sTitle" : "Gyro and Pita Sandwiches:Gyro and Pita Sandwiches" ,            
             "blurb" : "All served with lettuce & tomato"
        } ,#meta end

        "Ham_Cheese" : {#orderable item begin
            "iTitle" : "Ham & Cheese",
           "m_tags"  : (  "cheese", "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:5.50", ) ,
            }#pricing end
        } ,#orderable item end

        "Grilled_Chicken" : {#orderable item begin
            "iTitle" : "Grilled Chicken",
            "m_tags"  : ( "chicken", "non-vegetarian", ),
             "blurb" :  "includes cheese & honey mustard",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.00", ) ,
            }#pricing end
        } ,#orderable item end
        "Turkey_Cheese" : {#orderable item begin
            "iTitle" : "Turkey & Cheese",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:5.50", ) ,
            }#pricing end
        } ,#orderable item end
        "BLT" : {#orderable item begin
            "iTitle" : "BLT",
           "m_tags"  : ( "non-vegetarian" , "bacon" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:5.50", ) ,
            }#pricing end
        } ,#orderable item end
         "Chicken_Sovlaki" : {#orderable item begin
            "iTitle" : "Chicken Sovlaki",
           "m_tags"  : ( "non-vegetarian" , "chicken" ),
             "blurb" :  "includes onion & Tzatziki sauce",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.00", ) ,
            }#pricing end
        } ,#orderable item end
          "Gyro" : {#orderable item begin
            "iTitle" : "Gyro",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "includes onion & Tzatziki sauce",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:5.75", ) ,
            }#pricing end
        } ,#orderable item end
          "Chicken_Gyro" : {#orderable item begin
            "iTitle" : "Chicken Gyro",
           "m_tags"  : ( "non-vegetarian" , "chicken" ),
             "blurb" :  "includes onion & Tzatziki sauce",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:5.75", ) ,
            }#pricing end
        } ,#orderable item end
          "Vegetarian" : {#orderable item begin
            "iTitle" : "Vegetarian",
           "m_tags"  : ( "vegetarian" ),
             "blurb" :  "spinach, mushroom, broccoli, onion, & green pepper, with your choice of honey mustard or Tzatziki sauce",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:5.50", ) ,
            }#pricing end
        } ,#orderable item end
    }#section end
}#steak menu end
#---------------------------------------------
if  __name__ == '__main__' :
    import os,sys, pprint
    pp=pprint.PrettyPrinter(indent=4)
    pp.pprint (pitaSandMenu)
