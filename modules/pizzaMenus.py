#!/bin/env python2.7
#--------------------------------
# vi: sw=4 ts=4 expandtab ai
#--------------------------------
# $Id: pizzaMenus.py 70 2012-08-16 23:37:45Z pytools $
#--------------------------------


pizzaMenu = {
    "1_classic"   :   {        # section begin
       "meta" : {        # section meta begin
          "sTitle"  :  "Pizza:Your Pizza - Your way" ,
           "blurb"  :  """Our pizza is a masterpiece in itself.We serve only the finest ingredients. Award winning Wisconsin cheese blend, original sauce with herbs and spices, and dough made from scratch. The result is a light crispy crust baked in a pan for your enjoyment""",
            "sizes" :  "<b>small</b> serves 1-2  <b>large</b> serves 3-5"
       } ,    # section meta end

       "pizza"  : {      # orderable item begin
           "iTitle"  : "Pizza Choices" ,
           "blurb"   :  "",
           "pricing" : {   # options begin
               "m_tags"  : ( "vegetarian" ),
               "0_sizes" : ( "small:6.00", "large:9.00","X-Lg:12.00"),
               "1_types" : ( "thin crust:0", "regular crust:0" ) ,  
           "2m_toppings" : (
                      "Onions:1.0:2.0:3.0", "Peppers:1.0:2.0:3.0", "Olives:1.0:2.0:3.0", "Pepperoni:1.0:2.0:3.0", 
                      "Mushrooms:1.0:2.0:3.0", "Spinach:1.0:2.0:3.0", "Ham:1.0:2.0:3.0","Sausage:1.0:2.0:3.0",
                      "Ground Beef:1.0:2.0:3.0", "Meatball:1.0:2.0:3.0","Bacon:1.0:2.0:3.0","Anchovies:1.0:2.0:3.0",
                      "Tomato:1.0:2.0:3.0","Broccoli:1.0:2.0:3.0","Green Peppers:1.0:2.0:3.0" ,"Onions:1.0:2.0:3.0",
                      "Jalapeno Peppers:1.0:2.0:3.0","Pineapple:1.0:2.0:3.0","Extra cheese:1.0:2.0:3.0" 
                      
                      ) ,
           }  #  options end
        }     # orderable item end
    }         # section end
} 

#=================================================
gourmetPizzaMenu = { #gourmet menu begin
    "2_gourmetPizza"   :   {   # section begin
       "meta" : {      # meta begin
           "sTitle"  :  "Gourmet Pizza: Try a Mad Greek Gourmet Pizza" ,
           "blurb"  :  " " ,
            "sizes" :  "<b>small</b> serves 1-2,  <b>large</b> serves 3-5"
       } ,     # meta end

       "Vegetable_Supreme" : {   # orderable item begin
            "iTitle" : "Vegetable Supreme",
            "m_tags" : ("vegetarian",),
            "blurb" : "Sauce, Cheese, Mushroom, Green Pepper, onion, spinach, broccoli, & topped with fresh sliced tomato",
            "pricing" : {
                 "0_sizes" : ("small:9.00", "large:13.50", "X-Lg:17.00"),
                 "1_types" : ( "thin crust:0", "regular crust:0" ) ,
                
            },
        } ,      # orderable item end
        "Greek_Style_Pizza" : {
            "iTitle" : "Greek Style Pizza",
            "m_tags" : ("vegetarian",),
            "blurb" : "Garden style spinach, a blend of feta and pizza cheese, fresh sliced tomato (instead of sauce), black olives and a hint of fresh garlic.",
            "pricing" : {
                 "0_sizes" : ("small:8.00", "large:13.50","X-Lg:17.00"),
                 "1_types" : ("regular crust:0", "thin crust:0"),
                 "1_types" : ( "thin crust:0", "regular crust:0" ) ,
            
             },
        } , #orderable item end
        "Hawaiian_Style_Pizza" : {   # orderable item begin
            "iTitle" : "Hawaiian Style Pizza",
            "m_tags" : ("non-vegetarian",),
            "blurb" : "Sauce, cheese, pineapple, imported ham, & bits of bacon sprinkled on top.",
            "pricing" : {
                 "0_sizes" : ("small:8.00", "large:13.00", "X-Lg:16.00"),
                 "1_types" : ( "thin crust:0", "regular crust:0" ) ,
                
            },
        } ,      # orderable item end
        "Chicken_Delight" : {   # orderable item begin
            "iTitle" : "Chicken Delight",
            "m_tags" : ("non-vegetarian",),
            "blurb" : "Sauce, cheese, white chicken breast sauteed with fresh broccoli &  hint of fresh garlic",
            "pricing" : {
                 "0_sizes" : ("small:9.00", "large:13.50","X-Lg:17.00"),
                 "1_types" : ( "thin crust:0", "regular crust:0" ) ,
                
            },
        } ,      # orderable item end
        "White_Pizza" : {   # orderable item begin
            "iTitle" : "White Pizza",
            "m_tags" : ("vegetarian",),
            "blurb" : "Provolone cheese, mixed with our award winning Wisconsin cheese blend & a hint of fresh garlic",
            "pricing" : {
                 "0_sizes" : ("small:7.00", "large:9.00","X-Lg:13.00"),
                 "1_types" : ( "thin crust:0", "regular crust:0" ) ,
                
            },
        } ,      # orderable item end
        "Special_Pizza" : {   # orderable item begin
            "iTitle" : "Special Pizza",
            "m_tags" : ("non-vegetarian",),
            "blurb" : "Sauce, cheese, ground beef, sausage, pepperoni, mushroom, onion, anchovies & sweet peppers",
            "pricing" : {
                 "0_sizes" : ("small:9.00", "large:13.50","X-Lg:17.00"),
                 "1_types" : ( "thin crust:0", "regular crust:0" ) ,
                
            },
        } ,      # orderable item end
        "White_Primavera" : {   # orderable item begin
            "iTitle" : "White Primavera",
            "m_tags" : ("vegetarian",),
            "blurb" : "Our white pizza topped with fresh spinach, broccoli, & sliced tomato",
            "pricing" : {
                 "0_sizes" : ("small:9.00", "large:13.50","X-Lg:17.00"),
                 "1_types" : ( "thin crust:0", "regular crust:0" ) ,
                
            },
        } ,      # orderable item end
        "Meat_Lovers_Pizza" : {   # orderable item begin
            "iTitle" : "Meat Lovers Pizza",
            "m_tags" : ("non-vegetarian",),
            "blurb" : "Sauce, cheese, steak, pepperoni, ground beef, & sausage",
            "pricing" : {
                 "0_sizes" : ("small:9.25", "large:13.50","X-Lg:17.00"),
                 "1_types" : ( "thin crust:0", "regular crust:0" ) ,
                
            },
        } ,      # orderable item end
        "Cheese_Lovers_Pizza" : {   # orderable item begin
            "iTitle" : "Cheese Lovers Pizza",
            "m_tags" : ("vegetarian",),
            "blurb" : "Our delicious sauce with a combination of provolone, mozzarella, Wisconsin cheddar, & Parmesan cheese",
            "pricing" : {
                 "0_sizes" : ("small:7.00", "large:11.00","X-Lg:14.00"),
                 "1_types" : ( "thin crust:0", "regular crust:0" ) ,
                
            },
        } ,      # orderable item end
        "Buffalo_Chicken_Pizza" : {   # orderable item begin
            "iTitle" : "Buffalo Chicken Pizza",
            "m_tags" : ("non-vegetarian",),
            "blurb" : "Chicken, blue cheese, wing sauce, mozzarella cheese, & our original pizza sauce",
            "pricing" : {
                 "0_sizes" : ("small:9.00", "large:13.50","X-Lg:17.00"),
                 "1_types" : ( "thin crust:0", "regular crust:0" ) ,
             },   
        } ,      # orderable item end
        "BBQ_Chicken_Pizza" : {   # orderable item begin
            "iTitle" : "BBQ Chicken Pizza",
            "m_tags" : ("non-vegetarian",),
            "blurb" : "Chicken, mozzarella cheese, red onions, & BBQ sauce",
            "pricing" : {
                 "0_sizes" : ("small:9.00", "large:13.50","X-Lg:17.00"),
                 "1_types" : ( "thin crust:0", "regular crust:0" ) ,
             },   
        } ,      # orderable item end
        "Bacon_Cheeseburger_Pizza" : {   # orderable item begin
            "iTitle" : "Bacon Cheeseburger Pizza",
            "m_tags" : ("non-vegetarian",),
            "blurb" : "Bacon, ground beef, sauce, mozzarella, & American cheese",
            "pricing" : {
                 "0_sizes" : ("small:8.00", "large:12.50","X-Lg:16.00"),
                 "1_types" : ( "thin crust:0", "regular crust:0" ) ,
             },   
        } ,      # orderable item end
        "CheeseSteak_OR_Chicken_CheeseSteak_Pizza" : {   # orderable item begin
            "iTitle" : "CheeseSteak OR Chicken CheeseSteak Pizza",
            "m_tags" : ("non-vegetarian",),
            "blurb" : "Sauce, cheese, steak OR chickensteak",
            "pricing" : {
                 "0_sizes" : ("small:8.50", "large:13.50","X-Lg:17.00"),
                 "1_types" : ( "thin crust:0", "regular crust:0" ) ,
             },   
        } ,      # orderable item end
        "Salsa_Pizza" : {   # orderable item begin
            "iTitle" : "Salsa Pizza",
            "m_tags" : ("vegetarian",),
            "blurb" : "Salsa, sauce, tomato, onion, green peppers, fresh jalapeno, & cheese, all on a garlic baked crust",
            "pricing" : {
                 "0_sizes" : ("small:8.00", "large:12.00","X-Lg:16.00"),
                 "1_types" : ( "thin crust:0", "regular crust:0" ) ,
             },   
        } ,      # orderable item end
    }, #section end  
}# gourmet menu end

#=================================================
if  __name__ == '__main__' :
    import os,sys, pprint
    pp=pprint.PrettyPrinter(indent=4)
    pp.pprint (pizzaMenu)
    pp.pprint (gourmetPizzaMenu)
