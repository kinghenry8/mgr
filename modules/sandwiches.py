#------------------------------
#$Id: sandwiches.py 71 2012-08-19 19:32:25Z pytools $
#------------------------------
# vi: sw=4 ts=4 expandtab ai
#------------------------------
sandwichMenu =  {#pita menu begin
    "93_sandwiches" : {#section begin
        "meta" : {#meta begin
             "sTitle" : "Sandwiches:Sandwiches" ,            
             "blurb" : "All served with lettuce & tomato, with your choice of white or wheat bread, or a Kaiser roll. Extra cheese $0.50"
        } ,#meta end

        "Ham_Cheese" : {#orderable item begin
            "iTitle" : "Ham & Cheese",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:4.00", ) ,
            }#pricing end
        } ,#orderable item end

        "Tuna_Fish" : {#orderable item begin
            "iTitle" : "Tuna Fish",
            "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:4.00", ) ,
            }#pricing end
        } ,#orderable item end
        "Cheese" : {#orderable item begin
            "iTitle" : "Cheese",
           "m_tags"  : ( "vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:2.50", ) ,
            }#pricing end
        } ,#orderable item end
        "Grilled_Cheese_Tomato" : {#orderable item begin
            "iTitle" : "Grilled Cheese & Tomato",
           "m_tags"  : ( "vegetarian" , "cheese" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:3.25", ) ,
            }#pricing end
        } ,#orderable item end
         "Grilled_Cheese_Bacon" : {#orderable item begin
            "iTitle" : "Grilled Cheese & Bacon",
           "m_tags"  : ( "non-vegetarian" , ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:4.00", ) ,
            }#pricing end
        } ,#orderable item end
          "Sliders_with_Fries" : {#orderable item begin
            "iTitle" : "Sliders with Fries",
           "m_tags"  : ( "non-vegetarian" , ),
             "blurb" :  "3 mini chicken OR beef sandwiches",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
          "Turkey" : {#orderable item begin
            "iTitle" : "Turkey",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:4.00", ) ,
            }#pricing end
        } ,#orderable item end
          "BLT" : {#orderable item begin
            "iTitle" : "BLT",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:3.75", ) ,
            }#pricing end
        } ,#orderable item end
           "Grilled_Cheese" : {#orderable item begin
            "iTitle" : "Grilled Cheese",
           "m_tags"  : ( "vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:4.00", ) ,
            }#pricing end
        } ,#orderable item end
           "Grilled_Ham_Cheese" : {#orderable item begin
            "iTitle" : "Grilled Ham & Cheese",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:4.00", ) ,
            }#pricing end
        } ,#orderable item end
           "Grilled_Chicken_Sandwich" : {#orderable item begin
            "iTitle" : "Grilled Chicken Sandwich",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:5.50", ) ,
            }#pricing end
        } ,#orderable item end
    }#section end
}#steak menu end
#---------------------------------------------
if  __name__ == '__main__' :
    import os,sys, pprint
    pp=pprint.PrettyPrinter(indent=4)
    pp.pprint (sandwichMenu)
