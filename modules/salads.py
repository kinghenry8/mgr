#------------------------------
#$Id: salads.py 64 2012-06-10 23:59:04Z pytools $
#------------------------------
# vi: sw=4 ts=4 expandtab ai
#------------------------------
saladsMenu =  {#wings menu begin
    "96_salads" : {#section begin
        "meta" : {#meta begin
             "sTitle" : "Salads:Salads" ,            
             "blurb" : "Our freshly made salads are large enough for a hearty, healthy meal or can be shared for 2. <br> Includes iceberg lettuce, tomato,cucumber, carrot, a hard boiled egg, and dressing.<br> (Caesar, Russian, French, Italian, Greek, Blue Cheese, Ranch, or Honey Mustard)"
        } ,#meta end

        "Regular_cheese" : {#orderable item begin
            "iTitle" : "Regular (cheese)",
           "m_tags"  : ( "vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:4.50", ) ,
            }#pricing end
        } ,#orderable item end

        "Queen_Ham_Cheese" : {#orderable item begin
            "iTitle" : "Queen (Ham & Cheese)",
            "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:5.50", ) ,
            }#pricing end
        } ,#orderable item end
        "Chef_turkey_ham_cheese" : {#orderable item begin
            "iTitle" : "Chef (turkey, ham &cheese)",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:5.50", ) ,
            }#pricing end
        } ,#orderable item end
        "Greek_feta_cheese_olives)" : {#orderable item begin
            "iTitle" : "Greek with feta and olives",
           "m_tags"  : ( "vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.25", ) ,
            }#pricing end
        } ,#orderable item end
         "King_turkey_cheese)" : {#orderable item begin
            "iTitle" : "King (turkey & cheese)",
           "m_tags"  : ( "non-vegetarian" , ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:5.50", ) ,
            }#pricing end
        } ,#orderable item end
          "Tuna" : {#orderable item begin
            "iTitle" : "Tuna",
           "m_tags"  : ( "non-vegetarian" , ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:5.75", ) ,
            }#pricing end
        } ,#orderable item end
          "Grilled_Chicken" : {#orderable item begin
            "iTitle" : "Grilled Chicken",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end
          "EXTRA_DRESSING" : {#orderable item begin
            "iTitle" : "EXTRA_DRESSING",
           "m_tags"  : ( "vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:0.50", ) ,
            }#pricing end
        } ,#orderable item end
    }#section end
}#salads menu end
#---------------------------------------------
if  __name__ == '__main__' :
    import os,sys, pprint
    pp=pprint.PrettyPrinter(indent=4)
    pp.pprint (saladsMenu)
