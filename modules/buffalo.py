#------------------------------
#$Id: buffalo.py 71 2012-08-19 19:32:25Z pytools $
#------------------------------
# vi: sw=4 ts=4 expandtab ai
#------------------------------
wingsMenu =  {#wings menu begin
    "94_wings" : {#section begin
        "meta" : {#meta begin
             "sTitle" : "Buffalo Style Wings:Buffalo Style Wings" ,            
             "blurb" : "Choice of Sauce: BBQ, Spicy BBQ, Mild or Hot <br> All orders served with celery sticks & blue cheese dressing <br> EXTRA BLUE CHEESE: 0.65 cents each <br> **Also Available for catering**"
        } ,#meta end

        "10_pieces" : {#orderable item begin
            "iTitle" : "10 pieces (no split)",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50", ) ,
            }#pricing end
        } ,#orderable item end

        "20_pieces" : {#orderable item begin
            "iTitle" : "20 pieces (no split)",
            "m_tags"  : ( "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:12.50", ) ,
            }#pricing end
        } ,#orderable item end
        "30_pieces" : {#orderable item begin
            "iTitle" : "30 pieces (no split)",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:18.00", ) ,
            }#pricing end
        } ,#orderable item end
        "40_pieces" : {#orderable item begin
            "iTitle" : "40 pieces (no split)",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:23.50", ) ,
            }#pricing end
        } ,#orderable item end
         "50_pieces" : {#orderable item begin
            "iTitle" : "50 pieces",
           "m_tags"  : ( "non-vegetarian" , ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:29.00", ) ,
            }#pricing end
        } ,#orderable item end
          "60_pieces" : {#orderable item begin
            "iTitle" : "60 pieces",
           "m_tags"  : ( "non-vegetarian" , ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:34.50", ) ,
            }#pricing end
        } ,#orderable item end
           "70 pieces" : {#orderable item begin
            "iTitle" : "70 pieces",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:40.00", ) ,
            }#pricing end
        } ,#orderable item end
          "80 pieces" : {#orderable item begin
            "iTitle" : "80 pieces",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:45.50", ) ,
            }#pricing end
        } ,#orderable item end
           "90 pieces" : {#orderable item begin
            "iTitle" : "90 pieces",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:51.00", ) ,
            }#pricing end
        } ,#orderable item end
          "100_pieces" : {#orderable item begin
            "iTitle" : "100 pieces",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:57.00", ) ,
            }#pricing end
        } ,#orderable item end
    }#section end
}#steak menu end
#---------------------------------------------
if  __name__ == '__main__' :
    import os,sys, pprint
    pp=pprint.PrettyPrinter(indent=4)
    pp.pprint (wingsMenu)
