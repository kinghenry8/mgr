#------------------------------
#$Id: sides.py 71 2012-08-19 19:32:25Z pytools $
#------------------------------
# vi: sw=4 ts=4 expandtab ai
#------------------------------
sidesMenu =  {#sides menu begin
    "97_sides" : {#section begin
        "meta" : {#meta begin
             "sTitle" : "Side Orders:Side Orders" ,            
             "blurb" : ""
        } ,#meta end

        "French_Fries" : {#orderable item begin
            "iTitle" : "French Fries",
           "m_tags"  : ( "vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:2.50",) ,
            }#pricing end
        } ,#orderable item end

        "Pizza_Fries" : {#orderable item begin
            "iTitle" : "Pizza Fries",
            "m_tags"  : ( "vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:3.75",) ,
            }#pricing end
        } ,#orderable item end
        "Mega_Fries" : {#orderable item begin
            "iTitle" : "Mega Fries",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:4.50",) ,
            }#pricing end
        } ,#orderable item end
        "Cheese_Fries" : {#orderable item begin
            "iTitle" : "Cheese Fries",
           "m_tags"  : ( "vegetarian" , "cheese" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:3.25",) ,
            }#pricing end
        } ,#orderable item end
         "Mozzarella_Fries" : {#orderable item begin
            "iTitle" : "Mozzarella Fries",
           "m_tags"  : ( "vegetarian" , ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:3.50",) ,
            }#pricing end
        } ,#orderable item end
          "Onion_Rings" : {#orderable item begin
            "iTitle" : "Onion Rings",
           "m_tags"  : ( "vegetarian" , ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:2.25",) ,
            }#pricing end
        } ,#orderable item end
          "Cole_Slaw" : {#orderable item begin
            "iTitle" : "Cole Slaw",
           "m_tags"  : ( "vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:2.00",) ,
            }#pricing end
        } ,#orderable item end
          "6_Mozzarella_Sticks" : {#orderable item begin
            "iTitle" : "Mozzarella Sticks (6)",
           "m_tags"  : ( "vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:4.50",) ,
            }#pricing end
        } ,#orderable item end
           "5_Perogies" : {#orderable item begin
            "iTitle" : "Perogies (5)",
           "m_tags"  : ( "vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:5.25",) ,
            }#pricing end
        } ,#orderable item end
           "5_Jalapeno_Poppers" : {#orderable item begin
            "iTitle" : "Jalapeno Poppers (5)",
           "m_tags"  : ( "vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:5.25",) ,
            }#pricing end
        } ,#orderable item end
           "Chicken_nuggets" : {#orderable item begin
            "iTitle" : "Chicken Nuggets",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "with BBQ or honey mustard sauce",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:5.25",) ,
            }#pricing end
        } ,#orderable item end
         "Chicken_fingers" : {#orderable item begin
            "iTitle" : "fingers",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "with BBQ or honey mustard sauce",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:5.75",) ,
            }#pricing end
        } ,#orderable item end
         "Chicken_steak_quesadilla" : {#orderable item begin
            "iTitle" : "Chicken OR Steak Quesadilla",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "with fried onions, green peppers & with sour cream and salsa on the side",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.50" ,) ,
            }#pricing end
        } ,#orderable item end
          "Nachos" : {#orderable item begin
            "iTitle" : "Nachos",
           "m_tags"  : ( "vegetarian" ),
             "blurb" :  "with fresh jalapeno peppers, sour cream & cheese on the side",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:3.25", ) ,
            }#pricing end
        } ,#orderable item end
           "Side_Sampler" : {#orderable item begin
            "iTitle" : "Side Sampler",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "2 of each: mozzarella sticks, perogies, jalapeno poppers, & chicken fingers",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.25", ) ,
            }#pricing end
        } ,#orderable item en
    }#section end
}#sides menu end
#---------------------------------------------
if  __name__ == '__main__' :
    import os,sys, pprint
    pp=pprint.PrettyPrinter(indent=4)
    pp.pprint (sidesMenu)
