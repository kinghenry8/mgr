#------------------------------
#$Id: clubSand.py 64 2012-06-10 23:59:04Z pytools $
#------------------------------
# vi: sw=4 ts=4 expandtab ai
#------------------------------
clubSandMenu =  {#steak menu begin
    "90_clubSands" : {#section begin
        "meta" : {#meta begin
             "sTitle" : "Club Sandwiches:Club Sandwiches" ,            
             "blurb" : ""
        } ,#meta end

        "Ham_Cheese" : {#orderable item begin
            "iTitle" : "Ham & Cheese",
           "m_tags"  : (  "cheese", "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.00", ) ,
            }#pricing end
        } ,#orderable item end

        "Tuna_Fish" : {#orderable item begin
            "iTitle" : "Tuna Fish",
            "m_tags"  : ( "tuna", "non-vegetarian", ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.00", ) ,
            }#pricing end
        } ,#orderable item end
        "Turkey" : {#orderable item begin
            "iTitle" : "Turkey",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.00", ) ,
            }#pricing end
        } ,#orderable item end
        "Bacon" : {#orderable item begin
            "iTitle" : "Bacon",
           "m_tags"  : ( "non-vegetarian" ),
             "blurb" :  "",
           "pricing" :  {#pricing begin
              "0_sizes" : ("small:6.00", ) ,
            }#pricing end
        } ,#orderable item end
    }#section end
}#steak menu end
#---------------------------------------------
if  __name__ == '__main__' :
    import os,sys, pprint
    pp=pprint.PrettyPrinter(indent=4)
    pp.pprint (clubSandMenu)
    
