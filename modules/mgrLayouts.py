#!/bin/env python2.7
#--------------------------------
# vi: sw=4 ts=4 expandtab ai
# $Id: mgrLayouts.py 40 2012-03-25 18:50:58Z pytools $
#--------------------------------
import os,sys,pprint
import logging.handlers, logging.config
import time, inspect,re,pdb
from pyCustom import PyCustom
myTopDir=PyCustom.addOurPaths()
pp=pprint.PrettyPrinter(indent=4)
from infra.pyLibs import genUtils  as gu
from infra.pyLibs.cgiApp import CgiApp
from infra.pyLibs.clientUtils import ClientUtils

class MgrLayouts () :

    @staticmethod
    def  doTabLayout(tabsHtm) :
        print "Content-type: text/html\n"
        fh=open(tabsHtm)
        print fh.read()
        fh.close()

    #-------------------------------------
    @staticmethod
    def doOrderEntryLayout(oeLayHtm) :
        print "Content-type: text/html\n"
        fh=open(oeLayHtm)
        print fh.read()
        fh.close()

    #-------------------------------------
    @staticmethod
    def doFrontPage(fpageHtm) :
        print "Content-type: text/html\n"
        fh=open(fpageHtm)
        print fh.read()
        fh.close()
