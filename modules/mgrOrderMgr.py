#!/bin/env python2.7
#--------------------------------
# vi: sw=4 ts=4 expandtab ai
#--------------------------------
#$Id: mgrOrdEntry2.py 57 2012-05-19 03:33:58Z pytools $
#--------------------------------
import os,sys,pprint
#pp=pprint.PrettyPrinter(indent=4)
import logging.handlers, logging.config
import time, inspect,re,pdb
import shelve
from datetime import datetime
#-------- SDR imports ---------------
from pyCustom import PyCustom
myTopDir=PyCustom.addOurPaths()
from infra.pyLibs import genUtils  as gu
from infra.pyLibs.cgiApp import CgiApp
from infra.pyLibs.clientUtils import ClientUtils

import shelve

#--------------------------------
def  initWebApp() :
    print ClientUtils.emitSimpleHtml()
    print ClientUtils.emitJqHeaders()

    print """
    <html>
    <title> MGR Order Mgmt Panel</title>
    <style>
    .bone {
       font-family:  calibri;
       font-size:  14px;
       background-color:  #eda
    }

    .banner {
       font-family:  monotype corsiva;
       font-size:  20px;
       background-color:  #99c
    }

    legend {
       font-family:  calibri;
       font-size:  14px;
       background-color:  #ade
    }

    </style>
    <script>

    function fetchOrders() {
        nurl="?cgiAction=SHOW_ORDERS"
        $.ajax ( 
            {
                cache: false,
                dataType:   "html",
                url: nurl,
                success : function(data) {
                     $('#mainDiv').html (data)
                }
            }
        )
    }

    $(function() {
        fetchOrders()
        setInterval ("fetchOrders()", 10000 )
    } )

    </script>
    <body class='bone'>
    <div  id='titleDiv'>  
    <span class='banner'> &nbsp;  Mad Greek's Order Management Panel&nbsp;&nbsp;</span>
    </div>

    <div id='mainDiv'>
    </div>

    <div id='botDiv'>
    </div>

    """  
#--------------------------------
def __showOrdersInteractions() :
    print  """
<style>
    th {
       background-color: orange ;
       color: black ;
       font-size: 13px ;
    }
    td {
       font-size: 12px ;
       color: white;
    }
</style>

    <script>
    $(function() {
         $('legend').click( function() {
             $(this).siblings().toggle()
         } )
    })
    </script>
    """
#--------------------------------
def  showOrders() :
    print ClientUtils.emitSimpleHtml()
    print ClientUtils.emitJqHeaders()
    __showOrdersInteractions()
    shan2=shelve.open("UserOrders", writeback='false')
    shades=('#448844', '#446644')
    if shan2 != None :
        dt1=datetime.now()
        dtime1=dt1.strftime("%Y-%m-%d  %H:%M:%S")
        print "Last updated: %s<br>" % (dtime1)
        for userId in  shan2.keys() :
            print "<fieldset><legend><b><span style='background-color: #ccccee'>UserId:  %s</b></span></legend> " % (userId)
            orderNum=1
            print "<table style='font-family:Calibri; font-size: 12px; width: 98%;'>"
            print "<tr border=1> <th># <th> OrderId <th>phone <th>email <th> Delivery<br>Type  <th> Delivery<br>Addr <th> Delivery<br>Instructions<th> Pay Type <th> Total<br>w. Tax/Delivery <th> Order Details"
            for orderId in sorted ( shan2[userId] ) :
                orderData=shan2[userId][orderId]
                (email, phone, delmode, paymode, delAddr, delInstruct) = orderData[0].split('|')      # Order Details here 
                cartContents = orderData[1]
                shade=shades[orderNum%2]
                delAddr=delAddr.replace('::', '<br>')
                cartContents = orderData[1]
                shade=shades[orderNum%2]

                print "<tr bgcolor=%s><td>%d<td>%s<td>%s<td>%s<td>%s<td>%s<td>%s<td>%s" %  \
                (shade, orderNum, orderId, phone, email, delmode, delAddr, delInstruct, paymode)
                orderNum+=1


                #---- begin cart items -----
                grandTotal=sum (float(item['price']) for item in cartContents )
                print "<td>%5.2f<td>" % (grandTotal)
                itemNum=1
                #colors=('#ccaaee', '#aaeeaa')
                colors=('#884444', '#664444')
                print "<table style='font-family:Calibri; font-size: 12px;'>" 
                print "<tr bgcolor=#99aabb> <th># <th> Qty <th> Details <th> Price "
                cntr=0
                for item in cartContents :
                    detString=""
                    try :
                        details=item['details'].replace('|', "<br>")
                        details=details.replace('%20', ' ')
                        details=details.replace('_', ' ')
                        detString=re.sub (r'Section:\s*\d+ ', ' ', details)
                        detString=detString.strip(',')
                    except Exception,e :
                        detString=item['details']
                    ccolor=colors[cntr%2]
                    cntr+=1
                    print "<tr bgcolor=%s> <td> %d <td> %s <td> %s <td> %5.02f" % (
                        ccolor, itemNum , 
                        item['quantity'], detString, 
                        float (item['price'] ), 
                    ) 
                    itemNum +=1
                print "</table> "
                #---- end cart items -----
            print "</table></fieldset>"
#-----------------------------------------------------
validArgs= {
    "cgiAction" : {
        "cliKey" : "gAction",
        "help"   : "Allowed Actions",
        "type"   : "oneof|INIT_APP||SHOW_ORDERS",
        "sample" : "cgiAction SHOW_ORDERS",
        "default" : "INIT_APP"
    } , 
    "cgiUserId" : {
        "cliKey" : "gUserId",
        "help"   : "user id for cart",
        "type"   : "string" ,
        "sample" : "guest",
        "default" : "guest"
    } ,

}
#-----------------------------------
def processAppActions() :
    if  cliValues['gAction'] == 'INIT_APP' :
        initWebApp()
    elif  cliValues['gAction'] == 'SHOW_ORDERS' :
        showOrders()
#-----------------------------------
def processMain() :
    global cliValues
    if gu.showArgsHelp(validArgs)  :
        sys.exit(0)
    cliValues=CgiApp.processWebArgs(validArgs)
    if cliValues['gAction'] == 'help' :
        print ClientUtils.emitSimpleHtml()
        CgiApp.showHelp(cliValues['gScriptUrl'], validArgs)
        sys.exit(0)
    processAppActions()

#-----------------------------
if __name__ == "__main__" :
    logging.config.fileConfig(os.path.expanduser("~") + "/appConf/log4py.conf")
    processMain()
