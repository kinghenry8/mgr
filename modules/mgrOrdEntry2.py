#!/bin/env python2.7
#--------------------------------
# vi: sw=4 ts=4 expandtab ai
#--------------------------------
#$Id: mgrOrdEntry2.py 71 2012-08-19 19:32:25Z pytools $
#--------------------------------
import os,sys,pprint
#pp=pprint.PrettyPrinter(indent=4)
import logging.handlers, logging.config
import time, inspect,re,pdb, pudb
import shelve
#-------- SDR imports ---------------
from pyCustom import PyCustom
myTopDir=PyCustom.addOurPaths()
from infra.pyLibs import genUtils  as gu
from infra.pyLibs.cgiApp import CgiApp
from infra.pyLibs.clientUtils import ClientUtils

#--- Begin MGR App specific imports -------
from pizzaMenus import pizzaMenu, gourmetPizzaMenu
from stromboliMenus import stromboliMenu, gourmetStromboliMenu
from steakSand import steakSandMenu, whtSteakSandMenu
from hoagies import hoagiesMenu
from hamburgers import hamburgerMenu
from clubSand import clubSandMenu
from pitaSand import pitaSandMenu
from wraps import wrapsMenu
from sandwiches import sandwichMenu
from buffalo import wingsMenu
from pasta import pastaMenu
from salads import saladsMenu
from sides import sidesMenu
from desserts import dessertsMenu
from datetime import datetime, date 
from mgrSlides import  SlideManager

taxMulti=1.075
delCharge=2.0

class  MgrOrderEntry (object) :
    def __init__(self) :
        self.allMenus = dict(
        pizzaMenu.items() + gourmetPizzaMenu.items() + 
        stromboliMenu.items() + gourmetStromboliMenu.items() +
        steakSandMenu.items() + whtSteakSandMenu.items() +
        hoagiesMenu.items() + hamburgerMenu.items() +
        clubSandMenu.items() + pitaSandMenu.items() +
        wrapsMenu.items() + sandwichMenu.items() +
        wingsMenu.items() + pastaMenu.items() +
        saladsMenu.items() + sidesMenu.items() +
        dessertsMenu.items()
        ) 
    #----------------------------------------
    def renderTopMenu(self, userId='guest', script='myUrl')  :
        # print ClientUtils.emitJqHeaders()
        print """
        <body class='bone'>
    <style type='text/css'>
.secMenuItem {
   color:  #eeeecc ;
   font-size: 12pt;
}
    </style>
        <script>
            var userId='%s'
            $(".secMenuItem").click( function() {
                nurl=$(this).attr('link')
                nurl+="&cgiUserId=" + userId
                console.log ("LeftMenu ", nurl)
                $.ajax( 
                    {
                        url : nurl,
                        cache: false,
                        dataType: "html",
                        success: function (data) {
                            // console.log(data)
                            $('#secMenusDiv').html(data)
                        }
                    }
                )
            } ) 
        </script>
        """     %  (userId)
        smgr=SlideManager()         
        # smgr.initSlideManager()          # styles / js / div                      
        print smgr.initSlideFlipper()    # interaction  logic
        #smgr.initSlideFlipperMenu()      # clickable links

        print "<table  align=center width=80% cellpadding=0 cellspacing=1>"
        cnt=0
        for  section in sorted(self.allMenus.keys() ) :
            (shortTitle,longTitle)=self.allMenus[section]['meta']['sTitle'].split(':')
            blurb=self.allMenus[section]['meta']['blurb']
            secMenuLink="%s?cgiAction=SEC_MENU&cgiSecId=%s" % (script, section)
            print "<tr>"
            print "<td align=right><a href='#nogo' title='%s' link='%s' id='%s' class='secMenuItem'> %s </a> " % \
                    (blurb, secMenuLink, section, shortTitle)
            cnt+=1
        print "</table>"
    #-------------------------------------------------------
    def  refreshSession(self) :
        print """
<script type="text/javascript" src="/jsl/jq/cookie/jquery.cookie.js"></script>
<script>
    var cookieValue=$.cookie("mgrfoods")
    // alert (cookieValue)
    if  (cookieValue == null  ) {
        console.log ("Cookie no exist ..")
        $.cookie("mgrfoods", "guest_%d" )
        console.log("cookie  guest_%d  created" )
    } else {
        console.log("welcome back " +  cookieValue)  
    }
</script>
        """  % (guestNum, guestNum) 

    #------------------------------------------------------
    def refreshTopMenu(self,  userId='guest', script='selfUrl') :
        print "Content-type: text/html\n"
        print ClientUtils.emitJqHeaders()
        #self.refreshSession()
        self.renderTopMenu( userId=userId, script=script)

    #-------------------------------------------------------
    def refreshSession1(self ) :
        print """
<script type="text/javascript" src="/jsl/jq/jquery.cookies.2.2.0.js"></script>

        <script>
        $(function() {
           if ( $.cookies.test() ) {
               console.log("Browser supports cookies")
               cookValue=$.cookies.get("mgrfoods")
               if (cookValue == null) {
                    mgrOrdEntry2.guestNum+=1
                    $.cookie("mgrfoods", "guest_%d" % mgrOrdEntry2.guestNum)
                    console.log("cookie %s created" % mgrOrdEntry2.guestNum)
               } else {
                    console.log("welcome back %s" % cookValue)
               }
           } else {
              alert ("Please turn on Cookies")
           }
        } )
        </script>
        """
    #-------------------------------------------------------
    def  __separateSizedOptions(self, pricingElement='peppers:1.0:2.0', pickedSize='small') :
        pricedOpts= pricingElement.split(':')
        if len(pricedOpts) > 3 :
            (optName, smPrice, lgPrice, xlgPrice) = pricedOpts
        elif len(pricedOpts) > 2 :
            (optName, smPrice, lgPrice) = pricedOpts
        else :
            (optName, smPrice) = pricedOpts
        if pickedSize == 'xlarge' and  len(pricedOpts) > 3 :
            return (optName, xlgPrice)
        if pickedSize == 'large' and  len(pricedOpts) > 2 :
            return (optName, lgPrice)
        else :
            return (optName, smPrice)

    #-------------------------------------------------------
    def __spitSelectionGroup( self,
        groupName='someGroup', divSelector='#centerDiv2', selType='radio', 
        parentObj=None, section="", orderItem="", sizeType='oneSize'
        ) :
        print """
    <style>
    .selectors {
      font-size: 10pt;  
      font-family:'Tahoma' 
    }
    </style>
        """  
        btnSet=[]
        if isinstance(parentObj, tuple) :      
            childNum=0
            for  optElem in parentObj :
                (optName, thePrice) = self.__separateSizedOptions(optElem, pickedSize=sizeType)
                dispItem="%s:%s" % (optName, thePrice)
                itemLink="?cgiPriceItem=%s&cgiOrderItem=%s&cgiSecId=%s&cgiAction=PRICING" % \
                    ( dispItem, orderItem,  section)
                thisBtn={
                  "name" : dispItem , "value" : dispItem, "help" : "" , "checked": childNum == 0 and  selType=='radio'
                } 
                btnSet.append(thisBtn)
            childNum+=1
        else :                                   # is this not needed ?
            childNum=0
            for  childKey in parentObj.keys() :
                itemLink="?cgiOrderItem=%s&cgiSecId=%s&cgiAction=LEAF_MENU" % (childKey, section)
                if childKey != 'meta' :
                    thisBtn={
                      "name" : childKey , "value" : itemLink, "help" : "" , "checked": childNum == 0 and selType=='radio'
                    } 
                    btnSet.append(thisBtn)
                childNum+=1
        print ClientUtils.emitSelectButtons2(optionSet=btnSet, cssStyle='selectors', groupName=groupName, selectType=selType, numPerRow=4)
    #------------------------------------------------------
    def __secStyles(self) :
        print """
<style>
.secTitle {
  font-size: 14pt;  
  background-color: #66A ; 
  color: orange ;
  font-family:'monotype corsiva' 
}
.secBlurb {
  font-size: 10pt;  
  font-family:'Tahoma' 
}
th {
   background-color: orange ;
   color: black ;
   font-size: 13px ;
}
td {
   font-size: 12px ;
}

</style>
<link rel=sylesheet type=text/css href=jqSpin.css> 
        """
    #--------------------------------------------------------
    def __secInteractions(self, section="2_gourmetPizza", userId='guest') :
        print """
<script>
  var section='%s'
  var userId='%s'
    // alert ("[" + userId + "]")
    $(".dropDiv").click( function() {
        sizeType=$(this).attr('sizeType')
        orderable=$(this).attr('orderable')
        basePrice=$(this).attr('basePrice')
        qtyObjId=$(this).attr('qtyId')  
        console.log("CLICK1 qtyObjId",  qtyObjId, basePrice)
        qtyObj=document.getElementById(qtyObjId) 
        quantity=qtyObj.value
        nurl='?cgiAction=LEAF_MENU&cgiSecId='+section+'&cgiOrderItem='+orderable +'&cgiQuantity=' +quantity + '&cgiSizeType=' + sizeType
        nurl=nurl+'&cgiUserId='+userId
        console.log("Leafing  to: [", nurl, "]")
        var loadingGif="<img src='../../../infra/iPix/spinner2.gif' border=0>" 
        $('#extras').html("Working ... " + loadingGif)
        $('#extras').toggle(true)
        $.ajax( 
            {
                url : nurl,
                cache: false,
                dataType: "html",
                success: function (data) {
                    // console.log(data)
                    $('#extras').html(data)
                    console.log ( $('#extras').position()  )
                    $('#extras').css({ position: "absolute",
                                top: 150, left: 100 
                                });
                }
            }
        )
    } )
</script>
    """  % (section, userId)

#------------------------------------------------------
    def __renderSecThreeSizeRow(
        self, orderable, priceTypes, orderableKey, rcolor, 
        orderableTitle, orderableBlurb, add2CartPng, secObj
        ) :
        (smPrice,lgPrice, xlgPrice) = priceTypes
        smPrc=smPrice.split(':')[1]
        lgPrc=lgPrice.split(':')[1]
        xlgPrc=xlgPrice.split(':')[1]
        rowId='row_%s' % orderableKey    
        smCellId='sm_%s' % orderableKey
        lgCellId='lg_%s' % orderableKey
        xlgCellId='xlg_%s' % orderableKey
        smQtyId='smQty_%s' % orderableKey   # what is the id tag for the small quantity input
        lgQtyId='lgQty_%s' % orderableKey   # what is the id tag for the large quantity input
        xlgQtyId='xlgQty_%s' % orderableKey   # what is the id tag for the large quantity input

        smQtySpin="<input id=%s size=3 name=%s type='number' max='10' min='0' step='1' value='1'/>" % (smQtyId ,smQtyId)
        lgQtySpin="<input id=%s size=3 name=%s type='number' max='10' min='0' step='1'  value='1'/>" % (lgQtyId ,lgQtyId)
        xlgQtySpin="<input id=%s size=3 name=%s type='number' max='10' min='0' step='1'  value='1'/>" % (xlgQtyId ,xlgQtyId)


        smQtyDetails=""" %s <img 
            id=%s 
            orderable='%s' 
            sizeType='small' 
            qtyId='%s' 
            basePrice='%s'
            class='dropDiv' 
            title='Please enter Quantity and click for options' 
            src=%s> 
        """ % (smPrc,smCellId, orderable, smQtyId, smPrc, add2CartPng)

        lgQtyDetails=""" %s <img
            id=%s 
            orderable='%s' 
            sizeType='large' 
            qtyId='%s' 
            basePrice='%s' 
            class='dropDiv'
            title='Please enter Quantity and click for options'
            src=%s
            >
        """ % (lgPrc,lgCellId, orderable, lgQtyId, lgPrc, add2CartPng)

        xlgQtyDetails=""" %s <img
            id=%s 
            orderable='%s' 
            sizeType='xlarge' 
            qtyId='%s' 
            basePrice='%s' 
            class='dropDiv'
            title='Please enter Quantity and click for options'
            src=%s
            >
        """ % (xlgPrc,xlgCellId, orderable, xlgQtyId, xlgPrc, add2CartPng)

        smCell="%s %s" % (smQtySpin, smQtyDetails)
        lgCell="%s %s" % (lgQtySpin, lgQtyDetails)
        xlgCell="%s %s" % (xlgQtySpin, xlgQtyDetails)

        print "<tr id=%s rcolor=%s  title='%s' bgcolor=%s><td> %s<td>%s<td>%s<td>%s\n" % ( 
            rowId, rcolor, orderableBlurb, rcolor, secObj[orderable]['iTitle'], smCell, lgCell, xlgCell
            )

#------------------------------------------------------
    def __renderSecTwoSizeRow(
        self, orderable, priceTypes, orderableKey, rcolor, 
        orderableTitle, orderableBlurb, add2CartPng, secObj
        ) :
        (smPrice,lgPrice) = priceTypes
        smPrc=smPrice.split(':')[1]
        lgPrc=lgPrice.split(':')[1]
        rowId='row_%s' % orderableKey    
        smCellId='sm_%s' % orderableKey
        lgCellId='lg_%s' % orderableKey
        smQtyId='smQty_%s' % orderableKey   # what is the id tag for the small quantity input
        lgQtyId='lgQty_%s' % orderableKey   # what is the id tag for the large quantity input

        smQtySpin="<input id=%s size=3 name=%s type='number' max='10' min='0' step='1' value='1'/>" % (smQtyId ,smQtyId)
        lgQtySpin="<input id=%s size=3 name=%s type='number' max='10' min='0' step='1'  value='1'/>" % (lgQtyId ,lgQtyId)

        smQtyDetails=""" %s <img 
            id=%s 
            orderable='%s' 
            sizeType='small' 
            qtyId='%s' 
            basePrice='%s'
            class='dropDiv' 
            title='Please enter Quantity and click for options' 
            src=%s> 
        """ % (smPrc,smCellId, orderable, smQtyId, smPrc, add2CartPng)

        lgQtyDetails=""" %s <img
            id=%s 
            orderable='%s' 
            sizeType='large' 
            qtyId='%s' 
            basePrice='%s' 
            class='dropDiv'
            title='Please enter Quantity and click for options'
            src=%s
            >
        """ % (lgPrc,lgCellId, orderable, lgQtyId, lgPrc, add2CartPng)

        smCell="%s %s" % (smQtySpin, smQtyDetails)
        lgCell="%s %s" % (lgQtySpin, lgQtyDetails)

        print "<tr id=%s rcolor=%s  title='%s' bgcolor=%s><td> %s<td>%s<td>%s\n" % ( 
            rowId, rcolor, orderableBlurb, rcolor, secObj[orderable]['iTitle'], smCell, lgCell
            )

#------------------------------------------------------
    def __renderSecOneSizeRow(
        self, orderable, priceTypes, orderableKey, rcolor, 
        orderableTitle, orderableBlurb, add2CartPng, secObj
        ) :
        (smPrice) = priceTypes
        try :
            smPrc=smPrice[0].split(':')[1]
        except Exception,e:
            print ("Got %s : Check syntax of menus ..." % (e) )
        rowId='row_%s' % orderableKey    
        smCellId='sm_%s' % orderableKey
        lgCellId='lg_%s' % orderableKey
        smQtyId='smQty_%s' % orderableKey   # what is the id tag for the small quantity input
        lgQtyId='lgQty_%s' % orderableKey   # what is the id tag for the large quantity input

        smQtySpin="<input size=3 id=%s name=%s type='number' max='10' min='0' step='1' value='1'/>" % (smQtyId ,smQtyId)
        # smQtySpin="<input id=%s name=%s  value='1'>" % (smQtyId, smQtyId)

        smQtyDetails= """ %s <img 
            id=%s 
            sizeType='onesize' 
            orderable='%s' 
            qtyId='%s' 
            basePrice='%s' 
            title='Please enter Quantity and click for options'
            src='%s'
            class='dropDiv'
            > 
        """ % (smPrc,smCellId, orderableKey, smQtyId, smPrc, add2CartPng)
        smCell="%s %s" % (smQtySpin, smQtyDetails)

        print "<tr id=%s rcolor=%s title='%s' bgcolor=%s><td>%s<td>%s\n" % (
            rowId, rcolor, orderableBlurb, rcolor, orderableTitle, smCell
        )

#------------------------------------------------------
    def renderSectionMenu(self,  section="2_gourmetPizza", userId='guest') :
        #print "Content-type: text/html\n"
        self.__secStyles() 
        self.__secInteractions( section=section, userId=userId) 
        nurl='cgiSection=' + section  
        secObj=self.allMenus[section]
        (shortTitle,longTitle)=secObj['meta']['sTitle'].split(':')
        blurb=secObj['meta']['blurb']
        sizeMsg=secObj['meta'].get('sizes', "")
        print " <span class='secTitle'>&nbsp; %s &nbsp;</span>&nbsp;" % (longTitle)
        if sizeMsg != "" :
            print "<span class='secBlurb'> %s </span>" % ( sizeMsg)
        print "<br><span class='secBlurb'> %s </span>" %  (blurb)
        #250, 192, 71  => FA, C0, 47
        print "<table border=0  ; width=95% align=center>"
        orderable=secObj.keys()[1]    # sample one of the orderables to see if it has 2 sizes
        if orderable=='meta' :
            orderable=secObj.keys()[0]
        try :
            priceTypes=secObj[orderable]['pricing']['0_sizes']
            if len(priceTypes) > 2 :
                print "<tr><th>Item <th>Small Qty <th>Large Qty <th> Xlarge Qty"
            elif len(priceTypes) > 1 :
                print "<tr><th>Item <th>Small Qty <th>Large Qty"
            else :
                print "<tr><th>Item <th> Quantity"
            colors=('#444488', '#444466')
            rnum=0
            add2CartPng='/iPix/shopCart/PNG/18-add-to-cart-Shapes4FREE.png'

            for orderable in sorted(secObj.keys() ) :
                if orderable != "meta" :
                    rcolor=colors[rnum % 2] 
                    orderableTitle=secObj[orderable]['iTitle']
                    orderableKey=orderable.replace(' ', '_')
                    priceTypes=secObj[orderable]['pricing']['0_sizes']
                    orderableBlurb=secObj[orderable].get('blurb', 'No blurb provided')
                    if len(priceTypes) > 2 :          # has three sizes
                        self.__renderSecThreeSizeRow(orderable, priceTypes, orderableKey, rcolor, orderableTitle, orderableBlurb, add2CartPng, secObj)
                    elif len(priceTypes) > 1 :        # has two sizes
                        self.__renderSecTwoSizeRow(orderable, priceTypes, orderableKey, rcolor, orderableTitle, orderableBlurb, add2CartPng, secObj)
                    else :            # only one size
                        self.__renderSecOneSizeRow(orderable, priceTypes, orderableKey, rcolor, orderableTitle, orderableBlurb, add2CartPng, secObj )
                    rnum+=1
            print "</table>"

        except Exception,e :
            print ("Got %s" % (e) )


    #------------------------------------------------------
    def renderOnePricedOption(self, sizeType, pricedOption,  section, orderItem, divSelector) :
        re1=re.compile('\d+(m*)_(.*)') 
        m=re1.match(pricedOption)     # e.g.  1_types 2m_toppings 
        #print "sec=%s ordItem=%s pricedOption=%s" % (section, orderItem , pricedOption)
        optObj=self.allMenus[section][orderItem]['pricing'][pricedOption] 
        if  len (m.groups() )  > 1 :
            (multi, spItem) = (m.group(1), m.group(2) )
            if multi == 'm' :
                #print "%s : checkedbox"  % ( spItem)
                self.__spitSelectionGroup(
                    groupName=spItem, divSelector=divSelector, selType='checkbox' ,
                    parentObj=optObj, section=section, orderItem=orderItem, sizeType=sizeType
                )
            else :
                self.__spitSelectionGroup(
                    groupName=spItem, divSelector=divSelector, selType='radio' ,
                    parentObj=optObj,  section=section, orderItem=orderItem, sizeType=sizeType
                )
                #print "%s: radio"  % (spItem)
        else :
            print "ERROR ->> %s: radio"  % (pricedOption)

    #------------------------------------------------------
    def __leafInteractions(self, orderQty=1, basePrice="0.0", section="1_classic", 
        orderItem='pizza', sizeType='large',  userId='guest') :
        print ClientUtils.emitJqHeaders()
        (shortTitle,longTitle)=self.allMenus[section]['meta']['sTitle'].split(':')
        print """
    <script>
    $(function() {
      $('#toCart').click( function()  {
          var orderQty=%d
          var basePrice=%f
          var orderSum=""
          var section='%s'
          var orderItem='%s'
          var sizeType='%s'
          var userId='%s'
          console.log ("Adding2Cart  Qty=" + orderQty + " BasePrice=" + basePrice )
          try {
              var pizType=$('input:radio[name="types"]:checked').val()
              var pizzaType=""
              if ( pizType != undefined ) {
                  pizzaType=pizType.split(":")[0]
                  console.log ( "Pizza Type: " + pizzaType )
                  orderSum+="&pizzaType=" + pizzaType
              }

              var itemIns=$('#itemInstruct').val()
              var itemInstruct=escape(itemIns)
              var toppings= new Array()
              addPrice=0.0
              toppingList=""
              $(':checkbox[checked]').each ( function () {
                 oneTopping=$(this).val()
                 toppings.push ($(this).val() )
                 x1=oneTopping.split(":")
                 addPrice += parseFloat(x1[1] )
                 toppingList+=x1[0] + ","
              } )
              orderSum+="&addPrice=" + addPrice
              orderSum+="&addToppings=" + toppingList
              console.log (toppings, orderSum)

              itemsDetail=section + " > " + orderItem 
              if (sizeType != "onesize") {
                  itemsDetail+="|" + sizeType + ' > ' + pizzaType 
              }
              if ( toppingList != "" ) {
                  itemsDetail+='|Toppings: '+ toppingList
              }
              if ( itemInstruct != "" && itemInstruct != 'none' ) {
                  itemsDetail+='|Instructions:<br>' + itemInstruct
              }
              itemsPrice= (basePrice+addPrice) * orderQty
              nurl="?cgiAction=ADD2_CART&cgiQuantity=" + orderQty  + "&cgiItemsDetail=" + itemsDetail +  "&cgiItemsPrice="  + itemsPrice 
              nurl+="&cgiUserId=" + userId
              console.log(nurl)
              var loadingGif="<img src='../../../infra/iPix/spinner2.gif' border=0>" 
              $('#extras').prepend ("Pushing order to Cart ..." + loadingGif)
              // alert (nurl)
              $.ajax( 
                    {
                        url : nurl,
                        cache: false,
                        dataType: "html",
                        success: function (data) {
                            $('#cartDiv').html(data)
                            $('#extras').html("")
                            $('#extras').toggle(false)
                        }
                    }
              )

          } catch(err) {
             console.log ("Got: " +  err.message)
          }

      } ) 

      $('#noCart').click  ( function() {
           console.log ("clearing this") 
           $('#extras').html("")
           $('#extras').toggle(false)
      } )
    } )
    </script>
      """  % (orderQty, basePrice, shortTitle, orderItem, sizeType, userId) 

    #----------------------------------------------------------
    #  this is used to render the topping pop-up (one step before cart)
    def renderLeafMenu(self, section="1_classic", orderItem="pizza", sizeType='large', orderQty=1, userId='guest') :
        print "Content-type: text/html\n"
        print "<body style='font-family: Calibri'>"
        secTitle=self.allMenus[section]['meta']['sTitle'].split(':')[0]
        leafObj=self.allMenus[section][orderItem]
        priceTypes=leafObj['pricing']['0_sizes']


        if len(priceTypes) > 2 :
            (smPrice,lgPrice, xlgPrice) = priceTypes
            smPrc=smPrice.split(':')[1]
            lgPrc=lgPrice.split(':')[1]
            xlgPrc=xlgPrice.split(':')[1]
        elif len(priceTypes) > 1 :
            (smPrice,lgPrice) = priceTypes
            smPrc=smPrice.split(':')[1]
            lgPrc=lgPrice.split(':')[1]
        else :
            smPrice = priceTypes[0]
            smPrc=smPrice.split(':')[1]
        priceToShow=smPrc
        if sizeType=='large' :
            priceToShow=lgPrc
        elif sizeType=='xlarge' :
            priceToShow=xlgPrc

        self.__leafInteractions(int(orderQty), float(priceToShow), section, orderItem, sizeType, userId=userId )


        print """<span style='color:black'>
        <H4>%s - %s </H4> 
        <table>
        <tr><td align=right>Size choice : <td> %s
        <tr><td align=right>Quantity : <td> %s
        <tr><td align=right>Base Price : <td> %s
        </table>
        </span>""" % ( secTitle, leafObj['iTitle'], sizeType, orderQty, priceToShow)

        if leafObj.has_key('pricing') :
            divSet=('#divCenter3', '#divCenter4', '#divCenter5')
            i=0
            for  pricedOption  in   leafObj['pricing'] :
                if pricedOption != "m_tags" and pricedOption != "0_sizes" :
                    self.renderOnePricedOption (sizeType, pricedOption,  section, orderItem, divSelector=divSet[i])
                    i+=1

        print """
        <table>
        <tr><td>Special<br>Instructions <td> <textarea id='itemInstruct' rows=3 cols=40>none</textarea>
        </table>
        """
        print "<input type='button' id='toCart' value='addToCart'>"
        print "<input type='button' id='noCart' value='cancel'>"


    #----------------------------------------------------

    def __cartInteractions(self, userId='guest') :
        print """
    <script>
    $('#clearCart').click( function() {
        console.log ("Clearing Cart")
        nurl="?cgiAction=CLEAR_CART&cgiUserId=%s"
        $.ajax( 
            {
                url : nurl,
                cache: false,
                dataType: "html",
                success: function (data) {
                    // console.log(data)
                    $('#cartDiv').html(data)
                }
            }
        )
    } )
    </script>
    """  % (userId)

        print """
    <script>
    $('.delItem').click( function() {
        var itemNum=$(this).attr('itemId')
        nurl="?cgiAction=DEL_CART_ITEM&cgiUserId=%s&cgiItemId=" + itemNum
        console.log ("Delete Cart Item: ", itemNum, nurl)
        $.ajax( 
            {
                url : nurl,
                cache: false,
                dataType: "html",
                success: function (data) {
                    // console.log(data)
                    $('#cartDiv').html(data)
                }
            }
        )
    } )
    </script>
    """  % (userId)

        print """
    <script>
    $('#Checkout').click( function() {
        var loadingGif="<img src='../../../infra/iPix/spinner2.gif' border=0>" 
        $('#extras').html("Working ... " + loadingGif)
        nurl="?cgiAction=CHECKOUT_CART&cgiUserId=%s"
        console.log ("Checkout Cart: ", nurl)
        $('#extras').toggle(true)
        $.ajax( 
            {
                url : nurl,
                cache: false,
                dataType: "html",
                success: function (data) {
                    // console.log(data)
                    $('#extras').html(data)
                    $('#extras').css({ position: "absolute",
                                top: 100, left: 10 
                                });
                }
            }
        )
    } )
    </script>
    """   %  (userId)

    #----------------------------------------------------
    # the contents can come from an uncommited Cart or a Commited Order

    def __renderCartContents( self, cartContents=[], showActions=True ) :
        itemNum=1
        #colors=('#ccaaee', '#aaeeaa')
        colors=('#884444', '#664444')
        print "<div id='cartItems' style='max-height: 50%; overflow: auto; border: solid 1px orange'>"
        print "<table style='font-family:Calibri; font-size: 12px; width: 98%;'>"
        if showActions :
            print "<tr bgcolor=#99aabb> <th># <th> Qty <th> Details <th> Price <th>Actions"
        else :
            print "<tr bgcolor=#99aabb> <th># <th> Qty <th> Details <th> Price "

        cntr=0
        for item in cartContents :
            detString=""
            try :
                #details=item['details'].replace('onesize >', "")
                #details=details.replace('Toppings: |', "")
                #details=details.replace('Instructions: ', "Instructions:<br>")
                details=item['details'].replace('||', "")
                details=details.replace('\n', "<br>")
                details=details.replace('|', "<br>")
                details=details.replace('%20', ' ')
                details=details.replace('_', ' ')
                detString=details.strip(',')
            except Exception,e :
                detString=item['details']
            ccolor=colors[cntr%2]
            cntr+=1
            cartLink="""<img class='%s' itemId='%s' src='/iPix/shopCart/PNG/20-remove-from-cart-Shapes4FREE.png' title='Remove this item'> 
            """ % ('delItem', itemNum)
            if showActions :
                print "<tr bgcolor=%s> <td> %d <td> %s <td> %s <td> %5.02f <td>%s" % (
                ccolor, itemNum , 
                item['quantity'], detString, 
                float (item['price'] ), 
                cartLink 
                ) 
            else : 
                print "<tr bgcolor=%s> <td> %d <td> %s <td> %s <td> %5.02f" % (
                ccolor, itemNum , 
                item['quantity'], detString, 
                float (item['price'] ), 
                ) 
            itemNum +=1
        print "</table></div>"


    #----------------------------------------------------

    def renderUserCart(self, userId='guest') :
        self.__cartInteractions(userId=userId)
        print """
        <style>
        .secTitle {
          font-size: 14pt;  
          background-color: #66A ; 
          color: yellow ;
          font-family:'monotype corsiva' 
        }
        .userId {
            font-size: 10pt;
            color: green ;
            font-family:'calibri'
        }
        </style>
        <span class='secTitle'> &nbsp;&nbsp;  Cart for  User:</span>&nbsp;<span class=userId>%s </span> &nbsp;&nbsp;  """ % userId
        shan=shelve.open("UserCarts", writeback='false')
        if shan != None :
            if shan.has_key(userId) :
                cartContents=shan[userId]
                gTotal=sum (float(item['price']) for item in cartContents ) 
                grandTotal=gTotal*taxMulti
                print  "<p align=right> Total (incl Tax) :  <span style='font-size: 16pt; font-weight: bold'> %5.2f </span>"  % (grandTotal)
                print "<br><input type='button' id='clearCart' value='Clear'>"
                print "<input type='button' id='Checkout' value='Checkout'>"
                self.__renderCartContents(cartContents)
            shan.close()

    #--------------------------------------------------
    def addOneItemToCart(self, quantity=0, itemsDetail='someDetails', itemsPrice="0.0", userId='guest') :
        cartContents=[]
        shan=shelve.open("UserCarts", writeback='true')
        if shan != None :
            if shan.has_key(userId) :
                cartContents=shan[userId]

            print ClientUtils.emitSimpleHtml()
            print ClientUtils.emitJqHeaders()
            print "<body class='bone'>"
            itemToAdd={}
            itemToAdd['quantity'] = quantity
            itemToAdd['details'] = itemsDetail
            itemToAdd['price'] = itemsPrice
            cartContents.append( itemToAdd ) 
            shan[userId]=cartContents
        shan.close()
        self.renderUserCart(userId=userId)

    #--------------------------------------------------
    def  clearUserCart(self, userId='guest') :
        cartContents=[]
        shan=shelve.open("UserCarts", writeback='true')
        if shan != None :
            print ClientUtils.emitSimpleHtml()
            print ClientUtils.emitJqHeaders()
            print "<body class='bone'>"
            shan[userId]=cartContents
        shan.close()
        self.renderUserCart(userId=userId)
    #--------------------------------------------------
    def delCartItem (self, userId='guest', itemId=1) :
        toRemove=int(itemId)-1
        shan=shelve.open("UserCarts", writeback='true')
        if shan != None :
            if shan.has_key(userId) :
                userCartItems=shan[userId]
                if len(userCartItems) > toRemove and toRemove >=0 : 
                    userCartItems.pop(toRemove)
        shan[userId]=userCartItems
        shan.close()
        self.renderUserCart(userId=userId)

    #--------------------------------------------------
    def __generateUniqId (self) :
        pid=os.getpid()
        dt1=datetime.now()
        dtime=dt1.strftime("%Y%m%d_%H:%M:%S")
        uniqId="%s_%s" % (dtime,pid)
        return uniqId
    #--------------------------------------------------
    def generateUniqId (self, envBundle={}) :
        print "Content-type: text/plain\n"
        # ipaddr=envBundle['gRemAddr']
        uniqId=self.__generateUniqId()
        print uniqId,

    #----------------------------------------------------
    def __checkoutInteractions(self, userId='guest') :
        print """
    <script>
    var  userId='%s'
    var delCharge=%s
    $('#later').click( function() {
        $('#extras').toggle(false)
    } )


    function postCheckout(url, divObj) {
       $.ajax (
          {
              url:  url,
              cache: false,
              dataType: "html",
              success: function(data) {
                 // console.log(data)
                 if ( divObj != undefined ) {
                     divObj.html (data)
                 }

              }
          }
        )
    }

    $(function() {
        $("input[name='deltype']").change(function(){
            curVal=$(this).val()
            val1=$('#grandTotal').text()
            if ( curVal == 'deliver') {
               alert ("adding delivery charges to " + val1)
               val2=parseFloat(val1) + delCharge
               $('#grandTotal').html(val2.toFixed(2))
            } else {
               alert ("subtracting delivery charges from " + val1)
               val2=parseFloat(val1) - delCharge
               $('#grandTotal').html(val2.toFixed(2))
            }
        } )
    } )


    $('#confirm').click( function() {
        var email=$('#email').val()
        var phone1=$('#phone1').val()
        var phone2=$('#phone2').val()
        var phone3=$('#phone3').val()
        var deltype=$('input:radio[name="deltype"]:checked').val()
        var paytype=$('input:radio[name="pmtype"]:checked').val()
        var delAddr1=$('#delAddr1').val()
        var delAddr2=$('#delAddr2').val()
        var delAddr3=$('#delAddr3').val()
        var delAddr4=$('#delAddr4').val()
        var delAddr5=$('#delAddr5').val()
        delAddr=delAddr1+"::"+delAddr2+"::"+delAddr3+"::" +delAddr4+ "::" + delAddr5
        var delInstruct=$('#delInstruct').val()
        console.log ( "delType:", deltype, " payType:", paytype)
        $('#extras').toggle(false)
        orderInfo= email + "|" + phone1 + "-" + phone2 + "-" + phone3 + "|" + deltype + "|" + paytype + "|" + delAddr + "|" + delInstruct
        url1="?cgiAction=ORDER_COMMIT&cgiUserId=" + userId + "&cgiOrderInfo=" + orderInfo
        url2="?cgiAction=CLEAR_CART&cgiUserId=" + userId 
        url3="?cgiAction=SHOW_ORDERS&cgiUserId=" + userId 
        console.log (url1)

        $.ajax (
           {
              url:  url1 ,
              cache: false,
              dataType: "html",
              success: function(data) {
                  console.log(data)
                  ok=data.indexOf("OK:")
                  if (ok != -1 ) {
                      postCheckout(url2)
                      $('#cartDiv').html("")
                      console.log ("cleared cart")
                  }
                  postCheckout(url3, $('#ordersDiv') )
                  console.log ("updated ..",  ordersDiv)
              }
           }
        )
    } )
    </script>
        """    %  (userId, delCharge)
    #----------------------------------------------------
    def __checkoutContactInfo(self) :
        print """
    <table border=0>
    <tr><td align=right> email <td> <input id='email' type=text size=20> 
    <td align=right> payment mode <td > <input type=radio name='pmtype' value='credit'> Credit Card <br>
                                         <input type=radio name='pmtype' value='cash' checked> Cash 
    <tr><td align=right> phone <td> (<input id='phone1' type=text size=3 maxlength=3>) 
                                   - <input id='phone2' type=text size=3 maxlength=3> 
                                   - <input id='phone3' type=text size=4 maxlength=4> 
    <td align=right> delivery <td> <input type=radio name='deltype' value='pickup' > Pick-up <br>
                                       <input type=radio name='deltype' value='deliver' checked> Delivery 
    <tr><td align=right> instructions <td> <textarea id='delInstruct' row=3 cols=40 >No delivery instructions</textarea>
    <td align=right> Address <td> <input type=text id='delAddr1' value='123 MyStreet'>  <br>
                                  <input type=text id='delAddr2' value='Apt A1'> <br>
                                  <input type=text id='delAddr3' value='My Town'> <br>
                                  <select id='delAddr4'> 
                                      <option value='PA'>PA </option>
                                      <option value='DE'>DE</option>
                                      <option value='NJ'>NJ </option>
                                  </select>
                                  <input type=text id='delAddr5' value='ZIPCODE'>
    </table>
        """
    #----------------------------------------------------
    def checkoutCart(self, userId='guest') :
        shan=shelve.open("UserCarts", writeback='false')
        self.__checkoutInteractions (userId=userId)
        self.__checkoutContactInfo()
        if shan != None :
            if shan.has_key(userId) :
                cartContents=shan[userId]
                grandTotal=sum (float(item['price']) for item in cartContents )*taxMulti + delCharge
                print  "<p align=right> Total (incl Tax + delivery) :  <span id='grandTotal' style='font-size: 16pt; font-weight: bold'> %5.2f </span>"  % (grandTotal)
                print "<br><input type='button' id='later' value='Later'>"
                print " <input type='button' id='confirm' value='Confirm'>"
                self.__renderCartContents(cartContents, False)
            shan.close()
    #----------------------------------------------------
    def orderCommit(self, userId='guest',  orderInfo='order|details') :
        print  "Content-type: text/plain\n"
        confirmId=self.__generateUniqId()
        shan1=shelve.open("UserCarts", writeback='false')
        if shan1 != None :
            if shan1.has_key(userId) :
                shan2=shelve.open("UserOrders", writeback='true')
                if shan2 != None :
                    if not shan2.has_key(userId) :
                        shan2[userId]={}
                    shan2[userId][confirmId]=[]
                    shan2[userId][confirmId].append (orderInfo)
                    shan2[userId][confirmId].append (shan1[userId])
                    shan2.close()
                    print "OK: %s --> %s" %  (userId , confirmId) ,
                else :
                    print "ERR: Unable to open shelf for orders .."
            else :
                print "ERR:  No cart data for userId %s" % (userId)
        else :
            print "ERR: Unable to open shelf for reading cart data .."

    #----------------------------------------------------
    def renderOrdersForUserId(self, userId='guest') :
        print ClientUtils.emitSimpleHtml()
        print ClientUtils.emitJqHeaders()
        shan2=shelve.open("UserOrders", writeback='false')
        shades=('#448844', '#446644')
        if shan2 != None :
            if shan2.has_key(userId) :
                orderNum=1
                print "<table style='font-family:Calibri; font-size: 12px; width: 98%;'>"
                print "<tr border=1> <th># <th> OrderId <th>phone <th>email <th> Delivery<br>Type  <th> Delivery<br>Addr <th> Delivery<br>Instructions<th> Pay Type <th> Total<br>w. Tax/Delivery <th> Order Details"
                for orderId in sorted ( shan2[userId] ) :
                    orderData=shan2[userId][orderId]
                    (email, phone, delmode, paymode, delAddr, delInstruct) = orderData[0].split('|')      # Order Details here 
                    delAddr=delAddr.replace('::', '<br>')
                    cartContents = orderData[1]
                    shade=shades[orderNum%2]

                    print "<tr bgcolor=%s><td>%d<td>%s<td>%s<td>%s<td>%s<td>%s<td>%s<td>%s" %  \
                    (shade, orderNum, orderId, phone, email, delmode, delAddr, delInstruct, paymode)
                    orderNum+=1

                    #---- begin cart items -----
                    grandTotal=sum (float(item['price']) for item in cartContents )*taxMulti
                    if delmode == 'deliver' :
                        grandTotal+=delCharge
                    print "<td>%5.2f<td>" % (grandTotal)
                    itemNum=1
                    #colors=('#ccaaee', '#aaeeaa')
                    colors=('#884444', '#664444')
                    print "<table style='font-family:Calibri; font-size: 12px;'>" 
                    print "<tr bgcolor=#99aabb> <th># <th> Qty <th> Details <th> Price "
                    cntr=0
                    for item in cartContents :
                        detString=""
                        try :
                            details=item['details'].replace('|', "<br>")
                            details=details.replace('%20', ' ')
                            details=details.replace('_', ' ')
                            #detString=re.sub (r'Section:\s*\d+ ', ' ', details)
                            detString=details.strip(',')
                        except Exception,e :
                            detString=item['details']
                        ccolor=colors[cntr%2]
                        cntr+=1
                        print "<tr bgcolor=%s> <td> %d <td> %s <td> %s <td> %5.02f" % (
                            ccolor, itemNum , 
                            item['quantity'], detString, 
                            float (item['price'] ), 
                        ) 
                        itemNum +=1
                    print "</table>"
                    #---- end cart items -----
                print "</table>"
            else :
                print "No commited orders for userId: %s" % userId
    #------------------------------------------------------------

    def  initCarousel(self) :
        smgr=SlideManager()         
        smgr.initSlideManager()          # styles / js / div                      
        print smgr.initSlideFlipper()    # interaction  logic
        #smgr.initSlideFlipperMenu()      # clickable links
        smgr.initSlides()                # load slide images
